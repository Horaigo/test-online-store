﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace server.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        [Required]
        public String Products_id { get; set; }
        public int Price { get; set; }
        public DateTime Date { get; set; }
        public String Status { get; set; }

        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
    }
}