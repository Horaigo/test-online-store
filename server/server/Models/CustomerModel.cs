﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server.Models
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public string Restore_code { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Access_lvl { get; set; }
    }
}