﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server.Models
{
    public class News
    {
        public int NewsId { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}