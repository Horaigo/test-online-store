﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using server.Models;

namespace server.Models
{
    public class IndexViewModel
    {
        public IEnumerable <ProductModel> Products { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}