﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace server.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        [Required]
        public string Restore_code { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Access_lvl { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}