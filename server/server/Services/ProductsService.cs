﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using server.IServices;
using server.Models;

namespace server.Services
{
    public class ProductsService : IProductsService
    {
        ShopContext db = new ShopContext();

        public List<ProductModel> GetAllProducts()
        {
            var products = from b in db.Products
                           select new ProductModel()
                           {
                               ProductId = b.ProductId,
                               Name = b.Name,
                               Price = b.Price,
                               Description = b.Description,
                               Category = b.Category.Name
                           };
            return products.ToList();
        }

        public ShopContext GetShopContext()
        {
            return db;
        }
    }
}