﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using server.Models;
using server.IServices;

namespace server.Services
{
    public class CustomersService:ICustomersService
    {
        private ShopContext db = new ShopContext();

        public List<CustomerModel> GetAllCustomers()
        {
            var customers = from b in db.Customers
                            select new CustomerModel()
                            {
                                CustomerId = b.CustomerId,
                                Restore_code = b.Restore_code,
                                Username = b.Username,
                                Email = b.Email,
                                Password = b.Password,
                                Access_lvl = b.Access_lvl
                            };
            return customers.ToList();
        }

        public ShopContext GetShopContext()
        {
            return db;
        }
    }
}