﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using server.Models;
using server.IServices;

namespace server.Services
{
    public class CategoriesService:ICategoriesService
    {
        private ShopContext db = new ShopContext();

        public List<CategoryModel> GetAllCategories()
        {
            var categories = from b in db.Categories
                             select new CategoryModel()
                             {
                                 CategoryId = b.CategoryId,
                                 Name = b.Name,
                                 Description = b.Description
                             };
            return categories.ToList();
        }

        public ShopContext GetShopContext()
        {
            return db;
        }
    }
}