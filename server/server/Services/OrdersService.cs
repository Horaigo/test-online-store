﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using server.Models;
using server.IServices;

namespace server.Services
{
    public class OrdersService:IOrdersService
    {
        private ShopContext db = new ShopContext();

        public List<OrderModel> GetAllOrders()
        {
            var orders = from b in db.Orders
                         select new OrderModel()
                         {
                             OrderId = b.OrderId,
                             Products_id = b.Products_id,
                             Price = b.Price,
                             Date = b.Date,
                             Status = b.Status,
                             CustomerId = b.CustomerId
                         };
            return orders.ToList();
        }

        public ShopContext GetShopContext()
        {
            return db;
        }
    }
}