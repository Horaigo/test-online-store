﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using server.Models;
using server.IServices;

namespace server.Services
{
    public class NewsService:INewsService
    {
        private ShopContext db = new ShopContext();

        public IQueryable<News> GetAllNews()
        {
            return db.News;
        }

        public ShopContext GetShopContext()
        {
            return db;
        }
    }
}