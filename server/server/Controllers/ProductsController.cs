﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using server.Models;
using server.IServices;
using Ninject;

namespace server.Controllers
{
    [RoutePrefix("api/products")]
    public class ProductsController : ApiController
    {
        IProductsService iProductsService;

        public ProductsController()
        {
            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IServices.IProductsService>().To<Services.ProductsService>();
            iProductsService = ninjectKernel.Get<IProductsService>();
        }

        // GET: api/Products
        public List<ProductModel> GetProducts()
        {
            return iProductsService.GetAllProducts();
        }

        // GET: api/Products
        public IndexViewModel GetProductsPerPage(int page)
        {
            int pageSize = 5;
            List<ProductModel> productList = iProductsService.GetAllProducts();
            IEnumerable<ProductModel> productsPerPages = productList.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber=page, PageSize=pageSize, TotalItems=productList.Count};
            IndexViewModel ivm = new IndexViewModel { PageInfo=pageInfo, Products = productsPerPages};
            return ivm;
        }

        // GET: api/Products/5
        [ResponseType(typeof(ProductModel))]
        public async Task<IHttpActionResult> GetProduct(int id)
        {
            var product = await iProductsService.GetShopContext().Products.Include(b => b.Category).Select(b =>
                new ProductModel()
                {
                    ProductId = b.ProductId,
                    Name = b.Name,
                    Price = b.Price,
                    Description = b.Description,
                    Category = b.Category.Name
                }).SingleOrDefaultAsync(b => b.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                Console.WriteLine("Model");
                return BadRequest(ModelState);
            }

            if (id != product.ProductId)
            {
                Console.WriteLine("Id");
                return BadRequest();
            }

            iProductsService.GetShopContext().Entry(product).State = EntityState.Modified;

            try
            {
                await iProductsService.GetShopContext().SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    Console.WriteLine("Not Found");
                    return NotFound();
                }
                else
                {
                    Console.WriteLine("Duno");
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            iProductsService.GetShopContext().Products.Add(product);
            await iProductsService.GetShopContext().SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = product.ProductId }, product);
        }

        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> DeleteProduct(int id)
        {
            Product product = await iProductsService.GetShopContext().Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            iProductsService.GetShopContext().Products.Remove(product);
            await iProductsService.GetShopContext().SaveChangesAsync();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                iProductsService.GetShopContext().Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return iProductsService.GetShopContext().Products.Count(e => e.ProductId == id) > 0;
        }
    }
}