﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using server.Models;
using server.IServices;
using Ninject;

namespace server.Controllers
{
    public class CustomersController : ApiController
    {
        ICustomersService iCustomersService;

        public CustomersController()
        {
            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IServices.ICustomersService>().To<Services.CustomersService>();
            iCustomersService = ninjectKernel.Get<ICustomersService>();
        }

        // GET: api/Customers
        public List<CustomerModel> GetCustomers()
        {
            return iCustomersService.GetAllCustomers();
        }

        // GET: api/Customers/5
        [ResponseType(typeof(CustomerModel))]
        public async Task<IHttpActionResult> GetCustomer(int id)
        {
            var customer = await iCustomersService.GetShopContext().Customers.Include(b => b.Orders).Select(b =>
              new CustomerModel()
              {
                  CustomerId = b.CustomerId,
                  Restore_code = b.Restore_code,
                  Username = b.Username,
                  Email = b.Email,
                  Password = b.Password,
                  Access_lvl = b.Access_lvl
              }).SingleOrDefaultAsync(b => b.CustomerId == id);
            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // PUT: api/Customers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCustomer(int id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customer.CustomerId)
            {
                return BadRequest();
            }

            iCustomersService.GetShopContext().Entry(customer).State = EntityState.Modified;

            try
            {
                await iCustomersService.GetShopContext().SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Customers
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            iCustomersService.GetShopContext().Customers.Add(customer);
            await iCustomersService.GetShopContext().SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = customer.CustomerId }, customer);
        }

        // DELETE: api/Customers/5
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> DeleteCustomer(int id)
        {
            Customer customer = await iCustomersService.GetShopContext().Customers.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            iCustomersService.GetShopContext().Customers.Remove(customer);
            await iCustomersService.GetShopContext().SaveChangesAsync();

            return Ok(customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                iCustomersService.GetShopContext().Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(int id)
        {
            return iCustomersService.GetShopContext().Customers.Count(e => e.CustomerId == id) > 0;
        }
    }
}