﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using server.Models;
using server.IServices;
using Ninject;

namespace server.Controllers
{
    public class CategoriesController : ApiController
    {
        ICategoriesService iCategoriesService;

        public CategoriesController()
        {
            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IServices.ICategoriesService>().To<Services.CategoriesService>();
            iCategoriesService = ninjectKernel.Get<ICategoriesService>();
        }

        // GET: api/Categories
        public List<CategoryModel> GetCategories()
        {
            return iCategoriesService.GetAllCategories();
        }

        // GET: api/Categories/5
        [ResponseType(typeof(CategoryModel))]
        public async Task<IHttpActionResult> GetCategory(int id)
        {
            var category = await iCategoriesService.GetShopContext().Categories.Include(b => b.Products).Select(b =>
                new CategoryModel()
                {
                    CategoryId = b.CategoryId,
                    Name = b.Name,
                    Description = b.Description
                }).SingleOrDefaultAsync(b => b.CategoryId == id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCategory(int id, Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.CategoryId)
            {
                return BadRequest();
            }

            iCategoriesService.GetShopContext().Entry(category).State = EntityState.Modified;

            try
            {
                await iCategoriesService.GetShopContext().SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categories
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> PostCategory(Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            iCategoriesService.GetShopContext().Categories.Add(category);
            await iCategoriesService.GetShopContext().SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = category.CategoryId }, category);
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> DeleteCategory(int id)
        {
            Category category = await iCategoriesService.GetShopContext().Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            iCategoriesService.GetShopContext().Categories.Remove(category);
            await iCategoriesService.GetShopContext().SaveChangesAsync();

            return Ok(category);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                iCategoriesService.GetShopContext().Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return iCategoriesService.GetShopContext().Categories.Count(e => e.CategoryId == id) > 0;
        }
    }
}