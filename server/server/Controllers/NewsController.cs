﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using server.Models;
using server.IServices;
using Ninject;

namespace server.Controllers
{
    public class NewsController : ApiController
    {
        INewsService iNewsService;

        public NewsController()
        {
            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IServices.INewsService>().To<Services.NewsService>();
            iNewsService = ninjectKernel.Get<INewsService>();
        }

        // GET: api/News
        public IQueryable<News> GetNews()
        {
            return iNewsService.GetAllNews();
        }

        // GET: api/News/5
        [ResponseType(typeof(News))]
        public async Task<IHttpActionResult> GetNews(int id)
        {
            News news = await iNewsService.GetShopContext().News.FindAsync(id);
            if (news == null)
            {
                return NotFound();
            }

            return Ok(news);
        }

        // PUT: api/News/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutNews(int id, News news)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != news.NewsId)
            {
                return BadRequest();
            }

            iNewsService.GetShopContext().Entry(news).State = EntityState.Modified;

            try
            {
                await iNewsService.GetShopContext().SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/News
        [ResponseType(typeof(News))]
        public async Task<IHttpActionResult> PostNews(News news)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            iNewsService.GetShopContext().News.Add(news);
            await iNewsService.GetShopContext().SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = news.NewsId }, news);
        }

        // DELETE: api/News/5
        [ResponseType(typeof(News))]
        public async Task<IHttpActionResult> DeleteNews(int id)
        {
            News news = await iNewsService.GetShopContext().News.FindAsync(id);
            if (news == null)
            {
                return NotFound();
            }

            iNewsService.GetShopContext().News.Remove(news);
            await iNewsService.GetShopContext().SaveChangesAsync();

            return Ok(news);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                iNewsService.GetShopContext().Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NewsExists(int id)
        {
            return iNewsService.GetShopContext().News.Count(e => e.NewsId == id) > 0;
        }
    }
}