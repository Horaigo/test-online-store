﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using server.Models;
using server.IServices;
using Ninject;

namespace server.Controllers
{
    public class OrdersController : ApiController
    {
        IOrdersService iOrdersService;

        public OrdersController()
        {
            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IServices.IOrdersService>().To<Services.OrdersService>();
            iOrdersService = ninjectKernel.Get<IOrdersService>();
        }

        // GET: api/Orders
        public List<OrderModel> GetOrders()
        {
            return iOrdersService.GetAllOrders();
        }

        // GET: api/Orders/5
        [ResponseType(typeof(OrderModel))]
        public async Task<IHttpActionResult> GetOrder(int id)
        {
            var order = await iOrdersService.GetShopContext().Orders.Include( b => b.Customer).Select( b =>
                new OrderModel()
                {
                    OrderId = b.OrderId,
                    Products_id = b.Products_id,
                    Price = b.Price,
                    Date = b.Date,
                    Status = b.Status,
                    CustomerId = b.CustomerId
                }).SingleOrDefaultAsync( b => b.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        // PUT: api/Orders/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrder(int id, Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order.OrderId)
            {
                return BadRequest();
            }

            iOrdersService.GetShopContext().Entry(order).State = EntityState.Modified;

            try
            {
                await iOrdersService.GetShopContext().SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Orders
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> PostOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            iOrdersService.GetShopContext().Orders.Add(order);
            await iOrdersService.GetShopContext().SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = order.OrderId }, order);
        }

        // DELETE: api/Orders/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> DeleteOrder(int id)
        {
            Order order = await iOrdersService.GetShopContext().Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            iOrdersService.GetShopContext().Orders.Remove(order);
            await iOrdersService.GetShopContext().SaveChangesAsync();

            return Ok(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                iOrdersService.GetShopContext().Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return iOrdersService.GetShopContext().Orders.Count(e => e.OrderId == id) > 0;
        }
    }
}