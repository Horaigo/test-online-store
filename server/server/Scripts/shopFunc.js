﻿if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

var products;

$.ajax({
    url: 'api/products',
    type: 'GET',
    data: { page: 1 },
    success: function (data) {
        console.log(data);
        items = data.Products;
        content_holder.style["padding-bottom"] = "20px";
        list = document.getElementById("products_list");
        pages = document.getElementById("pages");
        for (let i = 1; i < 6; i++) {
            if (i - 1 >= items.length)
                break;
            var pageitem = [
                document.createElement('div'),
                document.createElement('div'),
                document.createElement('div'),
                document.createElement('img'),
                document.createElement('div'),
                document.createElement('div'),
                document.createElement('ins'),
                document.createElement('div'),
                document.createElement('a'),
                document.createElement('div'),
                document.createElement('h2'),
                document.createElement('a'),
                document.createElement('div')
            ];
            pageitem[0].className = "col-md-3 col-sm-6";
            pageitem[1].className = "single-shop-product";
            pageitem[2].className = "product-upper";
            pageitem[3].src = "Images/" + items[i - 1].Name + ".jpg";
            pageitem[3].onclick = function () { document.location.href = "../product.html?" + items[i - 1].ProductId; }
            pageitem[3].style.cursor = "pointer";
            pageitem[4].className = "product-price-cart";
            pageitem[5].className = "product-carousel-price";
            pageitem[6].textContent = "$" + items[i - 1].Price;
            pageitem[7].className = "product-option-shop";
            pageitem[8].className = "add_to_cart_button";
            if (getCookie("language") == "russian") {
                pageitem[8].textContent = "Добавить в корзину";
            }
            else {
                pageitem[8].textContent = "Add to cart";
            }
            pageitem[8].id = items[i - 1].ProductId;
            pageitem[8].onclick = function () { addToCart(this); }
            pageitem[9].className = "product-info";
            pageitem[11].textContent = items[i - 1].Name;
            pageitem[12].className = "product-description";
            pageitem[12].textContent = items[i - 1].Description;
            pageitem[2].appendChild(pageitem[3]);
            pageitem[5].appendChild(pageitem[6]);
            pageitem[7].appendChild(pageitem[8]);
            pageitem[4].appendChild(pageitem[5]);
            pageitem[4].appendChild(pageitem[7]);
            pageitem[1].appendChild(pageitem[2]);
            pageitem[1].appendChild(pageitem[4]);
            pageitem[11].href = "../product.html?" + items[i - 1].ProductId;
            pageitem[10].appendChild(pageitem[11]);
            pageitem[9].appendChild(pageitem[10]);
            pageitem[9].appendChild(pageitem[12]);
            pageitem[0].appendChild(pageitem[1]);
            pageitem[0].appendChild(pageitem[9]);
            list.appendChild(pageitem[0]);
        }
        pageCount = data.PageInfo.TotalPages;
        for (let i = 0; i < pageCount; i++) {
            var page = [
                document.createElement("li"),
                document.createElement("a")
            ];
            page[1].href = "#";
            page[1].onclick = function () {

                $.ajax({
                    url: 'api/products',
                    type: 'GET',
                    data: { page: i+1 },
                    success: function (data) {
                        let itemList = data.Products;
                        loadPageData(itemList)
                    }
                });
            };
            page[1].textContent = i + 1;
            page[0].appendChild(page[1]);
            pages.appendChild(page[0]);
        }
        if (getCookie("category") !== undefined) {
            index = getCookie("category");
            filterPageData(products, index);
            deleteCookie("category");
        }
    }
});

$.getJSON('api/products')
    .done(function (result) {
        products = result;
        filter_title = document.getElementById("filter_title");
        filter_title.onclick = function () { filterPageData(products, null); };
        gpu = document.getElementById("gpu");
        gpu.onclick = function () { filterPageData(products, "GPU"); };
        cpu = document.getElementById("cpu");
        cpu.onclick = function () { filterPageData(products, "CPU"); };
        powsup = document.getElementById("powsup");
        powsup.onclick = function () { filterPageData(products, "Power Supply"); };
        mboards = document.getElementById("mboards");
        mboards.onclick = function () { filterPageData(products, "Motherboards"); };
        compcase = document.getElementById("compcase");
        compcase.onclick = function () { filterPageData(products, "Computer Cases"); };
        ram = document.getElementById("ram");
        ram.onclick = function () { filterPageData(products, "RAM"); };
    });

var loadPageData = function (arr_data) {
    list = document.getElementById("products_list");
    list.innerHTML = "";
    if (arr_data.length == 0)
        return;
    for (let i = 1; i < 6; i++) {
        var pageitem = [
            document.createElement('div'),
            document.createElement('div'),
            document.createElement('div'),
            document.createElement('img'),
            document.createElement('div'),
            document.createElement('div'),
            document.createElement('ins'),
            document.createElement('div'),
            document.createElement('a'),
            document.createElement('div'),
            document.createElement('h2'),
            document.createElement('a'),
            document.createElement('div')
        ];
        pageitem[0].className = "col-md-3 col-sm-6";
        pageitem[1].className = "single-shop-product";
        pageitem[2].className = "product-upper";
        pageitem[3].src = "Images/" + arr_data[i - 1].Name + ".jpg";
        pageitem[3].onclick = function () { document.location.href = "../product.html?" + arr_data[i - 1].ProductId; }
        pageitem[3].style.cursor = "pointer";
        pageitem[4].className = "product-price-cart";
        pageitem[5].className = "product-carousel-price";
        pageitem[6].textContent = "$" + arr_data[i - 1].Price;
        pageitem[7].className = "product-option-shop";
        pageitem[8].className = "add_to_cart_button";
        pageitem[8].id = arr_data[i - 1].ProductId;
        pageitem[8].onclick = function () { addToCart(this); }
        if (getCookie("language") == "russian") {
            pageitem[8].textContent = "Добавить в корзину";
        }
        else {
            pageitem[8].textContent = "Add to cart";
        }
        pageitem[9].className = "product-info";
        pageitem[11].textContent = arr_data[i - 1].Name;
        pageitem[12].className = "product-description";
        pageitem[12].textContent = arr_data[i - 1].Description;
        pageitem[2].appendChild(pageitem[3]);
        pageitem[5].appendChild(pageitem[6]);
        pageitem[7].appendChild(pageitem[8]);
        pageitem[4].appendChild(pageitem[5]);
        pageitem[4].appendChild(pageitem[7]);
        pageitem[1].appendChild(pageitem[2]);
        pageitem[1].appendChild(pageitem[4]);
        pageitem[11].href = "../product.html?" + arr_data[i - 1].ProductId;
        pageitem[10].appendChild(pageitem[11]);
        pageitem[9].appendChild(pageitem[10]);
        pageitem[9].appendChild(pageitem[12]);
        pageitem[0].appendChild(pageitem[1]);
        pageitem[0].appendChild(pageitem[9]);
        list.appendChild(pageitem[0]);
    }
}

var filterPageData = function (arr_data, category) {
    var tmp = [];
    if (category != null) {
        for (let i = 0; i < arr_data.length; i++) {
            if (arr_data[i].Category == category)
                tmp.push(arr_data[i]);
        }
    }
    else
        tmp = arr_data;
    content_holder.style["padding-bottom"] = "20px";
    loadPageData(tmp);

    pages = document.getElementById("pages");
    pages.innerHTML = "";
    pageCount = tmp.length / 5;
    if (pageCount.toString().indexOf(".") != -1) {
        number = Number(pageCount.toString().substring(pageCount.toString().indexOf(".") + 1));
        if (number > 5)
            pageCount = Math.round(pageCount);
        else
            pageCount = Math.round(pageCount) + 1;
    }
    for (let i = 0; i < pageCount; i++) {
        var page = [
            document.createElement("li"),
            document.createElement("a")
        ];
        page[1].href = "#";
        let temp = [];
        for (let j = 0; j < 5; j++) {
            if ((i * 5 + j) < tmp.length)
                temp.push(tmp[i * 5 + j]);
        }
        page[1].onclick = function () { loadPageData(temp); };
        page[1].textContent = i + 1;
        page[0].appendChild(page[1]);
        pages.appendChild(page[0]);
    }
}

var addToCart = function (caller) {
    cart_price = document.getElementById("cart_price");
    cart_count = document.getElementById("cart_count");
    if (getCookie("cartItems") !== undefined) {
        str = getCookie("cartItems");
        str = str.split(",");
        str.push(caller.id);
        str = str.toString();
        document.cookie = "cartItems=" + str + ";path=/;";
        price = Number(getCookie("cartPrice"));
        price = price + products[caller.id - 1].Price;
        document.cookie = "cartPrice=" + price + ";path=/;";
        amount = Number(getCookie("cartCount")) + 1;
        document.cookie = "cartCount=" + amount + ";path=/;";
        cart_price.textContent = "$" + price;
        cart_count.textContent = amount.toString();
    }
    else {
        document.cookie = "cartItems=" + caller.id + ";path=/;";
        document.cookie = "cartCount=" + 1 + ";path=/;";
        document.cookie = "cartPrice=" + products[caller.id - 1].Price + ";path=/;";
        cart_price.textContent = "$" + products[caller.id - 1].Price;
        cart_count.textContent = "1";
    }
}

var searchData = function () {
    var tmp = [];
    search_string = (document.getElementById("search_field")).textContent;
    if (search_string != "") {
        products.forEach(function (value) {
            if ((value.Name.toLowerCase()).indexOf(search_string.toLowerCase()) != -1
                || ((value.Name.toLowerCase()).replace(/\s/g, '')).indexOf(search_string.toLowerCase()) != -1
                || (value.Name.toLowerCase()).indexOf((search_string.toLowerCase()).replace(/\s/g, '')) != -1
                || ((value.Name.toLowerCase()).replace(/\s/g, '')).indexOf((search_string.toLowerCase()).replace(/\s/g, '')) != -1) {
                tmp.push(value);
            }
        });
    }
    
    loadPageData(tmp);

    if (tmp.length == 0) {
        content_holder.style["padding-bottom"] = "150px";
        search_message_empty = document.createElement("h2");
        if (getCookie("language") == "russian") {
            search_message_empty.textContent = "Ничего не найдено.";
        }
        else {
            search_message_empty.textContent = "Nothing found.";
        }
        products_list.appendChild(search_message_empty);
    }

    pages = document.getElementById("pages");
    pages.innerHTML = "";
    pageCount = tmp.length / 5;
    if (pageCount != Math.round(pageCount))
        pageCount = Math.round(pageCount) + 1;
    for (let i = 0; i < pageCount; i++) {
        var page = [
            document.createElement("li"),
            document.createElement("a")
        ];
        page[1].href = "#";
        let temp = [];
        for (let j = 0; j < 5; j++) {
            if ((i * 5 + j) < tmp.length)
                temp.push(tmp[i*5+j]);
        }
        page[1].onclick = function () { loadPageData(temp); };
        page[1].textContent = i + 1;
        page[0].appendChild(page[1]);
        pages.appendChild(page[0]);
    }
}

function goToRussian() {
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";
    search_title.textContent = "Поиск товаров";
    search_button.textContent = "Поиск";
    filter_title_text.textContent = "Комплектующие ПК";
    gpu_title.textContent = "Видеокарты";
    cpu_title.textContent = "Процессоры";
    powsup_title.textContent = "Блоки питания";
    mboards_title.textContent = "Материнские карты";
    compcase_title.textContent = "Корпуса ПК";
    ram_title.textContent = "Оперативная память";
}