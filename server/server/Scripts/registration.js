﻿if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

var registerUser = function () {
    fname = document.getElementById("fname");
    fname = fname.value;
    email = document.getElementById("email");
    email = email.value;
    email_conf = document.getElementById("email_conf");
    email_conf = email_conf.value;
    username = document.getElementById("username");
    username = username.value;
    password = document.getElementById("password");
    password = password.value;
    password_conf = document.getElementById("password_conf");
    password_conf = password_conf.value;
    email_alert = document.getElementById("email_alert");
    email_alert.style.visibility = "hidden";
    password_conf_alert = document.getElementById("password_alert");
    password_conf_alert.style.visibility = "hidden";
    password_conf_alert = document.getElementById("password_conf_alert");
    password_conf_alert.style.visibility = "hidden";
    email_conf_alert = document.getElementById("email_conf_alert");
    email_conf_alert.style.visibility = "hidden";
    register_welcome = document.getElementById("register_welcome");
    register_welcome.style.visibility = "hidden";
    username_alert = document.getElementById("username_alert");
    username_alert.style.visibility = "hidden";
    recovery.style.visibility = "hidden";
    if (/^(\S)+(@yandex.ru|@mail.ru|@gmail.com|@rambler.com)\b$/.test(email) && /^(\S)+(@yandex.ru|@mail.ru|@gmail.com|@rambler.com)\b$/.test(email_conf)) {

        var email_promise = checkEmail(email);
        email_promise.then(
            result => {
                if (result == null) {

                    if (email_conf == email) {

                        if (username != "") {
                            var username_promise = checkUsername(username);
                            username_promise.then(
                                result => {

                                    if (result == null && username != "") {

                                        if (password != "" || password_conf != "") {

                                            if (password == password_conf) {

                                                if (fname != "") {
                                                    register_welcome = document.getElementById("register_welcome");
                                                    register_welcome.style.display = "block";
                                                    $.post('api/customers', { Restore_code: fname, Username: username, Password: password, Email: email, Access_lvl: 1 })
                                                        .done(function () {
                                                            register_button = document.getElementById("register-button");
                                                            register_button.style.display = "none";
                                                            document.location.href = "../login.html";
                                                        });
                                                }
                                                else {
                                                    if (getCookie("language") == "russian") {
                                                        recovery.textContent = "Пожалуйста заполните это поле";
                                                    }
                                                    else {
                                                        recovery.textContent = "Please, fill this field";
                                                    }
                                                    recovery.style.visibility = "visible"
                                                }
                                            }
                                            else {
                                                password_conf_alert = document.getElementById("password_conf_alert");
                                                password_conf_alert.style.visibility = "visible"
                                            }

                                        }
                                        else {
                                            password_conf_alert = document.getElementById("password_alert");
                                            password_conf_alert.style.visibility = "visible"
                                        }

                                    }
                                    else {
                                        username_alert = document.getElementById("username_alert");
                                        if (getCookie("language") == "russian") {
                                            username_alert.textContent = "Это имя пользователя уже занято.";
                                        }
                                        else {
                                            username_alert.textContent = "This username is already taken by another user.";
                                        }
                                        username_alert.style.visibility = "visible"
                                    }

                                },
                                error => {}
                            );
                        }
                        else {
                            username_alert = document.getElementById("username_alert");
                            if (getCookie("language") == "russian") {
                                username_alert.textContent = "Имя пользователя не может быть пустым.";
                            }
                            else {
                                username_alert.textContent = "Username cannot be empty.";
                            }
                            username_alert.style.visibility = "visible"
                        }
                    }
                    else {
                        email_conf_alert = document.getElementById("email_conf_alert");
                        email_conf_alert.style.visibility = "visible"
                    }

                }
                else {
                    email_alert = document.getElementById("email_alert");
                    if (getCookie("language") == "russian") {
                        email_alert.textContent = "Этот адрес электронной почты уже занят.";
                    }
                    else {
                        email_alert.textContent = "This email is already taken.";
                    }
                    email_alert.style.visibility = "visible"
                }
            },
            error => {}
        );
        

    }
    else {
        email_alert = document.getElementById("email_alert");
        if (getCookie("language") == "russian") {
            email_alert.textContent = "Пожалуйста, введите правильный адрес электронной почты.";
        }
        else {
            email_alert.textContent = "Please, enter correct email address.";
        }
        email_alert.style.visibility = "visible"
    }
}

var checkUsername = function (nickname) {
    var promise = new Promise((resolve, reject) => {
        $.getJSON('api/customers')
            .done(function (data) {
                for (i = 0; i < data.length; i++) {
                    if (data[i].Username == nickname) {
                        tmp = i;
                        break;
                    }
                    else
                        tmp = null;
                }
                resolve(tmp);
            });
    });
    return promise;
}

var checkEmail = function (mail) {
    var promise = new Promise((resolve, reject) => {
        $.getJSON('api/customers')
            .done(function (data) {
                for (i = 0; i < data.length; i++ ) {
                    if (data[i].Email == mail) {
                        tmp = i;
                        break;
                    }
                    else
                        tmp = null;
                }
                resolve(tmp);
            });
    });
    return promise;
}

function goToRussian() {
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";

    page_title.textContent = "Регистрация";
    reg_title.textContent = "Регистрация";
    reg_welcome.textContent = "Пожалуйста заполните все необходимые поля.";
    reg_name.textContent = "Код восстановления (Это поможет вам восстановить ваш аккаунт в случае утери):";
    reg_pass.textContent = "Пароль:";
    reg_email.textContent = "Электронная почта:";
    reg_email_conf.textContent = "Подтверждение электронной почты:";
    reg_username.textContent = "Имя пользователя:";
    reg_pass_conf.textContent = "Подтверждение пароля:";
    (document.getElementById("register-button")).textContent = "Зарегистрироваться";
    email_alert.textContent = "Пожалуйста, введите правильный адрес электронной почты.";
    email_conf_alert.textContent = "Введенные адреса электронной почты не совпадают.";
    username_alert.textContent = "Данное имя пользователя уже занято.";
    password_alert.textContent = "Поля для паролей не могут быть пусты.";
    password_conf_alert.textContent = "Введенные пароли не совпадают.";
    register_welcome.textContent = "Вы успешно зарегистрировались в нашем магазине."
}