﻿if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

function goToRussian(){
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";

    page_title.textContent = "Восстановление пароля";
    restore_title.textContent = "Восстановление утерянного пароля.";
    restore_message_email.textContent = "Пожалуйста введите адрес электренной почты, использованный при регистрации:";
    restore_message_email.style["font-size"] = "12px";
    restore_message_code.textContent = "Пожалуйста введите код восстановления вашего аккаунта:";
    restore_message_code
.style["font-size"] = "12px";
    restore_button.textContent = "Восстановить пароль";
}

function restorePass() {
    code_alert.style.visibility = "hidden";
    email_alert.style.visibility = "hidden";
    $.getJSON('api/customers')
        .done(function (data) {
            
            for (i = 0; i < data.length; i++) {
                if (data[i].Email == email_restore.value && data[i].Restore_code == code_restore.value) {
                    recovery_data.innerHTML = "";
                    user_password = document.createElement("div");
                    if (getCookie("language") == "russian") {
                        user_password.textContent = "Ваш пароль: " + data[i].Password;
                    }
                    else {
                        user_password.textContent = "Your password: " + data[i].Password;
                    }
                    recovery_data.appendChild(user_password);
                    go_to_login = document.createElement("div");
                    go_to_login.className = "register-button";
                    if (getCookie("language") == "russian") {
                        go_to_login.textContent = "Перейти к экрану входа";
                    }
                    else {
                        go_to_login.textContent = "Go to login screen";
                    }
                    go_to_login.onclick = function () {
                        document.location.href = "login.html";
                    };
                    recovery_data.appendChild(go_to_login);
                    return;
                }
                if (data[i].Email == email_restore.value && data[i].Restore_code != code_restore.value) {
                    if (getCookie("language") == "russian") {
                        code_alert.textContent = "Неверный код восстановления.";
                    }
                    code_alert.style.visibility = "visible";
                    return;
                }
            }
            if (getCookie("language") == "russian") {
                email_alert.textContent = "Пользователь с таким эл. адресом не зарегистрирован.";
            }
            email_alert.style.visibility = "visible";
        });
}