﻿if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

var user;
var products = [];
var orders = [];
var customers = [];
var categories = [];
var orders_full = [];
if (Number(getCookie("accLvl")) != 1) {
    tab_4.style.visibility = "visible";
    label_4.style.visibility = "visible";
}
$.getJSON('api/customers/' + getCookie("userId"))
    .done(function (data) {
        user = data;
        fname.textContent = user.Restore_code;
        username.textContent = user.Username;
        email.textContent = user.Email;
    });
$.getJSON('api/products')
    .done(function (list) {
        products = list;

        order_list = document.getElementById("orders");
        $.getJSON('api/orders')
            .done(function (data) {
                orders_full = data;
                data.forEach(function (item) {
                    if (item.CustomerId == getCookie("userId")) {
                        orders.push(item);
                    }
                });

                if (orders.length == 0) {
                    order_message = document.createElement('div');
                    if (getCookie("language") == "russian") {
                        order_message.textContent = "Вы еще не сделали ни одного заказа.";
                    }
                    else {
                        order_message.textContent = "You have not made any orders yet.";
                    }
                    order_list.appendChild(order_message);
                }

                orders.forEach(function (item) {
                    prod_in_order = (item.Products_id).split(",");
                    prod_in_order = prod_in_order.reduce(function (acc, el) {
                        acc[el] = (acc[el] || 0) + 1;
                        return acc;
                    }, {});

                    ordertable = [
                        document.createElement('table'),
                        document.createElement('tr'),
                        document.createElement('th'),
                        document.createElement('tr'),
                        document.createElement('th'),
                        document.createElement('th'),
                        document.createElement('th'),
                        document.createElement('th'),
                    ];
                    ordertable[0].className = "shop_table cart";
                    ordertable[2].setAttribute("colspan", 4);
                    date = item.Date;
                    date = (date.toString()).split("T");
                    date[1] = date[1].substring(0, 8);
                    date = date[0] + ", " + date[1];
                    date = date.replace("-", ".");
                    date = date.replace("-", ".");
                    ordertable[2].textContent = date;
                    ordertable[4].className = "product-name";
                    if (getCookie("language") == "russian") {
                        ordertable[4].textContent = "Товар";
                    }
                    else {
                        ordertable[4].textContent = "Product";
                    }
                    ordertable[5].className = "product-price";
                    if (getCookie("language") == "russian") {
                        ordertable[5].textContent = "Цена";
                    }
                    else {
                        ordertable[5].textContent = "Price";
                    }
                    ordertable[6].className = "product-quantity";
                    if (getCookie("language") == "russian") {
                        ordertable[6].textContent = "Количество";
                    }
                    else {
                        ordertable[6].textContent = "Quantity";
                    }
                    ordertable[7].className = "product-subtotal";
                    if (getCookie("language") == "russian") {
                        ordertable[7].textContent = "Общая сумма";
                    }
                    else {
                        ordertable[7].textContent = "Total";
                    }
                    ordertable[1].appendChild(ordertable[2]);
                    ordertable[3].appendChild(ordertable[4]);
                    ordertable[3].appendChild(ordertable[5]);
                    ordertable[3].appendChild(ordertable[6]);
                    ordertable[3].appendChild(ordertable[7]);
                    ordertable[0].appendChild(ordertable[1]);
                    ordertable[0].appendChild(ordertable[3]);

                    for (key in prod_in_order) {
                        tableItem = [
                            document.createElement('tr'),
                            document.createElement('td'),
                            document.createElement('span'),
                            document.createElement('td'),
                            document.createElement('span'),
                            document.createElement('td'),
                            document.createElement('span'),
                            document.createElement('td'),
                            document.createElement('span')
                        ];
                        tableItem[0].className = "cart_item";
                        tableItem[1].className = "product-name";
                        tableItem[2].textContent = products[key-1].Name;
                        tableItem[2].onclick = function () { document.location.href = "../product.html?" + products[key-1].ProductId; };
                        tableItem[3].className = "product-price";
                        tableItem[4].className = "amount";
                        tableItem[4].textContent = "$" + products[key-1].Price + ".00";
                        tableItem[5].className = "product-quantity";
                        tableItem[6].className = "amount";
                        tableItem[6].textContent = prod_in_order[key];
                        tableItem[7].className = "product-subtotal";
                        tableItem[8].className = "amount";
                        tableItem[8].textContent = "$" + (prod_in_order[key] * products[key - 1].Price) + ".00";
                        tableItem[1].appendChild(tableItem[2]);
                        tableItem[3].appendChild(tableItem[4]);
                        tableItem[5].appendChild(tableItem[6]);
                        tableItem[7].appendChild(tableItem[8]);
                        tableItem[0].appendChild(tableItem[1]);
                        tableItem[0].appendChild(tableItem[3]);
                        tableItem[0].appendChild(tableItem[5]);
                        tableItem[0].appendChild(tableItem[7]);
                        ordertable[0].appendChild(tableItem[0]);
                    }
                    order_list.appendChild(ordertable[0]);
                    order_status = document.createElement('div');
                    order_status.className = "total-price";
                    status_text = document.createElement('span');
                    status_text.className = "total-price";
                    status_text.textContent = item.Status;
                    total = document.createElement('span');
                    total.className = "total-price";

                    if (item.Status == "Accepted") {
                        status_text.style.color = "green";
                    }
                    if (item.Status == "In processing") {
                        status_text.style.color = "gold";
                    }
                    if (item.Status == "Declined") {
                        status_text.style.color = "red";
                    }

                    if (getCookie("language") == "russian") {
                        total.textContent = "Всего: $" + item.Price;
                        order_status.textContent = "Статус заказа: ";
                        if (item.Status == "Accepted") {
                            status_text.textContent = "Принят";
                        }
                        if (item.Status == "In processing") {
                            status_text.textContent = "В обработке";
                        }
                        if (item.Status == "Declined") {
                            status_text.textContent = "Отклонен";
                        }
                    }
                    else {
                        total.textContent = "Total: $" + item.Price;
                        order_status.textContent = "Status: ";
                    }
                    order_list.appendChild(order_status);
                    order_status.appendChild(status_text);
                    order_list.appendChild(total);

                });
                $.getJSON('api/customers')
                    .done(function (user_list) {
                        customers = user_list;
                        addUser();
                        buildUsersTable();
                        addProduct();
                        $.getJSON('api/categories')
                            .done(function (categories_list) {
                                categories = categories_list;
                                buildProductsTable();
                                buildNewsTable();
                            });
                    });
                orders_acception();
            });
    });
txt_1.style.display = "block";
tab_1.onclick = function () {
    txt_1.style.display = "block";
    txt_2.style.display = "none";
    txt_3.style.display = "none";
    txt_4.style.display = "none";
}
tab_2.onclick = function () {
    txt_1.style.display = "none";
    txt_2.style.display = "block";
    txt_3.style.display = "none";
    txt_4.style.display = "none";
}
tab_3.onclick = function () {
    txt_1.style.display = "none";
    txt_2.style.display = "none";
    txt_3.style.display = "block";
    txt_4.style.display = "none";
}
tab_4.onclick = function () {
    txt_1.style.display = "none";
    txt_2.style.display = "none";
    txt_3.style.display = "none";
    txt_4.style.display = "block";
}

var checkPassword = function () {
    var curPass = (document.getElementById("oldpass")).value;
    var newPass = (document.getElementById("newpass")).value;
    var message = document.getElementById("alert-message");
    message.style.visibility = "hidden";
    if (curPass == "") {
        if (getCookie("language") == "russian") {
            message.textContent = "Пожалуйста введите свой текущий пароль прежде чем менять его на новый.";
        }
        else {
            message.textContent = "Please enter your current password before changing it to a new one.";
        }
        message.style.visibility = "visible";
        return false;
    }
    if (curPass != user.Password) {
        if (getCookie("language") == "russian") {
            message.textContent = "Неверный текущий пароль.";
        }
        else {
            message.textContent = "Invalid current password.";
        }
        message.style.visibility = "visible";
        return false;
    }
    if (newPass == "") {
        if (getCookie("language") == "russian") {
            message.textContent = "Новый пароль не может быть пустым.";
        }
        else {
            message.textContent = "New password cannot be empty.";
        }
        message.style.visibility = "visible";
        return false;
    }
    if (newPass == curPass) {
        if (getCookie("language") == "russian") {
            message.textContent = "Новый пароль не должен совпадать с текущим.";
        }
        else {
            message.textContent = "The new password should not coincide with the current one.";
        }
        message.style.visibility = "visible";
        return false;
    }
    return true;
}

var changePassword = function () {
    if (checkPassword()) {
        $.ajax({
            url: 'api/customers/' + getCookie("userId"),
            type: 'PUT',
            data: { CustomerId: user.CustomerId, Restore_code: user.Restore_code, Email: user.Email, Access_lvl: user.Access_lvl, Username: user.Username, Password: (document.getElementById("newpass")).value },
            success: function (result) {
                document.location.reload();
            }
        });
        
    }
}

var buildUsersTable = function () {

    usertable = [
        document.createElement('table'),
        document.createElement('tr'),
        document.createElement('th'),
        document.createElement('tr'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th')
    ];

    usertable[0].className = "shop_table cart";
    usertable[0].id = "table-user";
    if (Number(getCookie("accLvl")) != 4) {
        usertable[0].style.display = "none";
    }
    usertable[2].setAttribute("colspan", 8);
    if (getCookie("language") == "russian") {
        usertable[2].textContent = "Пользователи";
    }
    else {
        usertable[2].textContent = "Users";
    }

    usertable[4].className = "product-remove";

    if (getCookie("language") == "russian") {
        usertable[4].textContent = "Удалить";
    }
    else {
        usertable[4].textContent = "Remove";
    }
    usertable[5].className = "product-price";
    if (getCookie("language") == "russian") {
        usertable[5].textContent = "Номер";
    }
    else {
        usertable[5].textContent = "User ID";
    }
    usertable[6].className = "product-price";
    if (getCookie("language") == "russian") {
        usertable[6].textContent = "Имя клиента";
    }
    else {
        usertable[6].textContent = "Username";
    }
    usertable[7].className = "product-price";
    if (getCookie("language") == "russian") {
        usertable[7].textContent = "Код восст.";
    }
    else {
        usertable[7].textContent = "Recovery code";
    }
    usertable[8].className = "product-price";
    if (getCookie("language") == "russian") {
        usertable[8].textContent = "Эл. почта";
    }
    else {
        usertable[8].textContent = "Email";
    }
    usertable[9].className = "product-price";
    if (getCookie("language") == "russian") {
        usertable[9].textContent = "Пароль";
    }
    else {
        usertable[9].textContent = "Password";
    }
    usertable[10].className = "product-price";
    if (getCookie("language") == "russian") {
        usertable[10].textContent = "Ур. доступа (1-4)";
    }
    else {
        usertable[10].textContent = "Access level (1-4)";
    }
    usertable[11].className = "product-price";
    if (getCookie("language") == "russian") {
        usertable[11].textContent = "Подтверждение";
    }
    else {
        usertable[11].textContent = "Confirm changes";
    }

    usertable[1].appendChild(usertable[2]);
    usertable[3].appendChild(usertable[4]);
    usertable[3].appendChild(usertable[5]);
    usertable[3].appendChild(usertable[6]);
    usertable[3].appendChild(usertable[7]);
    usertable[3].appendChild(usertable[8]);
    usertable[3].appendChild(usertable[9]);
    usertable[3].appendChild(usertable[10]);
    usertable[3].appendChild(usertable[11]);
    usertable[0].appendChild(usertable[1]);
    usertable[0].appendChild(usertable[3]);

    customers.forEach(function (customer) {

        tableItem = [
            document.createElement('tr'),
            document.createElement('td'),
            document.createElement('a'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div')
        ];

        tableItem[0].className = "cart_item";
        tableItem[0].id = "user" + customer.CustomerId;
        tableItem[1].className = "product-remove";
        tableItem[2].className = "remove";
        if (getCookie("language") == "russian") {
            tableItem[2].title = "Удалить";
        }
        else {
            tableItem[2].title = "Remove";
        }
        tableItem[2].textContent = "X";
        tableItem[2].id = "delete" + customer.CustomerId;
        tableItem[2].onclick = function () {
            
            var owner = Number(this.id.substring(6));
            var user_orders = [];

            $.ajax({
                url: 'api/customers/' + owner,
                type: 'DELETE',
                success: function (res) {
                    orders_full.forEach(function (value) {
                        if (value.CustomerId == owner) {
                            user_orders.push(value.OrderId);
                        }
                    });

                    if (user_orders.length == 0)
                        document.location.reload();

                    user_orders.forEach(function (value) {
                        $.ajax({
                            url: 'api/orders/' + value,
                            type: 'DELETE',
                            success: function (result) {
                                document.location.reload();
                            }
                        });
                    });
                }
            });
        };
        tableItem[3].className = "product-price";
        tableItem[4].textContent = customer.CustomerId;
        tableItem[5].className = "product-price";
        tableItem[6].className = "edit-data";
        tableItem[6].id = "username" + customer.CustomerId;
        tableItem[6].setAttribute("contenteditable", true);
        tableItem[6].textContent = customer.Username;
        tableItem[7].className = "product-price";
        tableItem[8].className = "edit-data";
        tableItem[8].id = "code" + customer.CustomerId;
        tableItem[8].setAttribute("contenteditable", true);
        tableItem[8].textContent = customer.Restore_code;
        tableItem[9].className = "product-price";
        tableItem[10].className = "edit-data";
        tableItem[10].id = "email" + customer.CustomerId;
        tableItem[10].setAttribute("contenteditable", true);
        tableItem[10].textContent = customer.Email;
        tableItem[11].className = "product-price";
        tableItem[12].className = "edit-data";
        tableItem[12].id = "pass" + customer.CustomerId;
        tableItem[12].setAttribute("contenteditable", true);
        tableItem[12].textContent = customer.Password;
        tableItem[13].className = "product-price";
        tableItem[14].className = "edit-data";
        tableItem[14].id = "level" + customer.CustomerId;
        tableItem[14].setAttribute("contenteditable", true);
        tableItem[14].textContent = customer.Access_lvl;
        tableItem[15].className = "product-price edit-user";
        if (getCookie("language") == "russian") {
            tableItem[15].textContent = "Сохранить";
        }
        else {
            tableItem[15].textContent = "Save changes";
        }
        tableItem[15].id = "edit-user" + customer.CustomerId;
        tableItem[15].onclick = function () {
            client_id = Number(this.id.substring(9));
            client = {
                CustomerId: client_id,
                Username: (document.getElementById('username' + client_id)).textContent,
                Restore_code: (document.getElementById('code' + client_id)).textContent,
                Email: (document.getElementById('email' + client_id)).textContent,
                Password: (document.getElementById('pass' + client_id)).textContent,
                Access_lvl: (document.getElementById('level' + client_id)).textContent
            }

            if (getCookie("accLvl") != client.Access_lvl && customer.CustomerId == Number(getCookie('userId'))) {
                document.cookie = "accLvl=" + client.Access_lvl + ";path=/;";
            }
            $.ajax({
                url: 'api/customers/' + client_id,
                type: 'PUT',
                data: client,
                success: function (result) {
                    document.location.reload();
                }
            });
        };
        if (getCookie("language") == "russian") {
            tableItem[16].textContent = "Это вы";
        }
        else {
            tableItem[16].textContent = "It's you";
        }

        if (customer.CustomerId == Number(getCookie('userId')))
            tableItem[1].appendChild(tableItem[16]);
        else
            tableItem[1].appendChild(tableItem[2]);
        tableItem[3].appendChild(tableItem[4]);
        tableItem[5].appendChild(tableItem[6]);
        tableItem[7].appendChild(tableItem[8]);
        tableItem[9].appendChild(tableItem[10]);
        tableItem[11].appendChild(tableItem[12]);
        tableItem[13].appendChild(tableItem[14]);
        tableItem[0].appendChild(tableItem[1]);
        tableItem[0].appendChild(tableItem[3]);
        tableItem[0].appendChild(tableItem[5]);
        tableItem[0].appendChild(tableItem[7]);
        tableItem[0].appendChild(tableItem[9]);
        tableItem[0].appendChild(tableItem[11]);
        tableItem[0].appendChild(tableItem[13]);
        tableItem[0].appendChild(tableItem[15]);
        usertable[0].appendChild(tableItem[0]);
    });
    admin_panel.appendChild(usertable[0]);
}

var buildProductsTable = function () {
    prodtable = [
        document.createElement('table'),
        document.createElement('tr'),
        document.createElement('th'),
        document.createElement('tr'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th')
    ];

    prodtable[0].className = "shop_table cart";
    prodtable[0].id = "table-products";
    if (Number(getCookie("accLvl")) != 3 && Number(getCookie("accLvl")) != 4) {
        prodtable[0].style.display = "none";
    }
    prodtable[2].setAttribute("colspan", 6);
    if (getCookie("language") == "russian") {
        prodtable[2].textContent = "Продукты";
    }
    else {
        prodtable[2].textContent = "Products";
    }

    prodtable[4].className = "product-price";
    if (getCookie("language") == "russian") {
        prodtable[4].textContent = "Номер";
    }
    else {
        prodtable[4].textContent = "Product ID";
    }
    prodtable[5].className = "product-price";
    if (getCookie("language") == "russian") {
        prodtable[5].textContent = "Название";
    }
    else {
        prodtable[5].textContent = "Product Name";
    }
    prodtable[6].className = "product-price";
    if (getCookie("language") == "russian") {
        prodtable[6].textContent = "Цена";
    }
    else {
        prodtable[6].textContent = "Price";
    }
    prodtable[7].className = "product-price";
    if (getCookie("language") == "russian") {
        prodtable[7].textContent = "Описание";
    }
    else {
        prodtable[7].textContent = "Description";
    }
    prodtable[8].className = "product-price";
    if (getCookie("language") == "russian") {
        prodtable[8].textContent = "Категория";
    }
    else {
        prodtable[8].textContent = "Category";
    }
    prodtable[9].className = "product-price";
    if (getCookie("language") == "russian") {
        prodtable[9].textContent = "Подтверждение";
    }
    else {
        prodtable[9].textContent = "Confirm changes";
    }

    prodtable[1].appendChild(prodtable[2]);
    prodtable[3].appendChild(prodtable[4]);
    prodtable[3].appendChild(prodtable[5]);
    prodtable[3].appendChild(prodtable[6]);
    prodtable[3].appendChild(prodtable[7]);
    prodtable[3].appendChild(prodtable[8]);
    prodtable[3].appendChild(prodtable[9]);
    prodtable[0].appendChild(prodtable[1]);
    prodtable[0].appendChild(prodtable[3]);

    products.forEach(function (product) {

        tableItem = [
            document.createElement('tr'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
            document.createElement('div'),
            document.createElement('td'),
        ];

        tableItem[0].className = "cart_item";
        tableItem[0].id = "product" + product.ProductId;
        tableItem[1].className = "product-price td-width";
        tableItem[2].textContent = product.ProductId;
        tableItem[3].className = "product-price td-width";
        tableItem[4].className = "edit-product-data";
        tableItem[4].setAttribute("contenteditable", true);
        tableItem[4].id = "item-name" + product.ProductId;
        tableItem[4].textContent = product.Name;
        tableItem[5].className = "product-price td-width";
        tableItem[6].className = "edit-product-data";
        tableItem[6].setAttribute("contenteditable", true);
        tableItem[6].id = "item-price" + product.ProductId;
        tableItem[6].textContent = "$" + product.Price + ".00";
        tableItem[7].className = "product-price td-width";
        tableItem[8].className = "edit-product-data";
        tableItem[8].setAttribute("contenteditable", true);
        tableItem[8].id = "item-desc" + product.ProductId;
        tableItem[8].textContent = product.Description;
        tableItem[9].className = "product-price td-width";
        tableItem[10].id = "category" + product.ProductId;
        tableItem[10].textContent = product.Category;
        tableItem[11].className = "product-price edit-user td-width";
        if (getCookie("language") == "russian") {
            tableItem[11].textContent = "Сохранить";
        }
        else {
            tableItem[11].textContent = "Save changes";
        }
        tableItem[11].id = "edit-product" + product.ProductId;
        tableItem[11].onclick = function () {
            product_id = this.id.substring(12);
            product_price = (document.getElementById("item-price" + product_id)).textContent;
            product_price = product_price.substring(1);
            product_price = product_price.split(".");
            product_price = Number(product_price[0]);
            var product_cat;

            categories.forEach(function (key) {
                if (key.Name == (document.getElementById("category" + product_id)).textContent) {
                    product_cat = key.CategoryId;
                }
            });

            goody = {
                ProductId: product_id,
                Name: (document.getElementById("item-name" + product_id)).textContent,
                Price: product_price,
                Description: (document.getElementById("item-desc" + product_id)).textContent,
                CategoryId: product_cat
            }

            $.ajax({
                url: 'api/products/' + product_id,
                type: 'PUT',
                data: goody,
                success: function (result) {
                    document.location.reload();
                }
            });
        };
        tableItem[1].appendChild(tableItem[2]);
        tableItem[3].appendChild(tableItem[4]);
        tableItem[5].appendChild(tableItem[6]);
        tableItem[7].appendChild(tableItem[8]);
        tableItem[9].appendChild(tableItem[10]);
        tableItem[0].appendChild(tableItem[1]);
        tableItem[0].appendChild(tableItem[3]);
        tableItem[0].appendChild(tableItem[5]);
        tableItem[0].appendChild(tableItem[7]);
        tableItem[0].appendChild(tableItem[9]);
        tableItem[0].appendChild(tableItem[11]);
        prodtable[0].appendChild(tableItem[0]);
    });
    admin_panel.appendChild(prodtable[0]);
}

var addUser = function () {
    usertable = [
        document.createElement('table'),
        document.createElement('tr'),
        document.createElement('th'),
        document.createElement('tr'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th')
    ];

    usertable[0].className = "shop_table cart";
    usertable[0].id = "addUser";
    if (Number(getCookie("accLvl")) != 4) {
        usertable[0].style.display = "none";
    }
    usertable[2].setAttribute("colspan", 6);
    if (getCookie("language") == "russian") {
        usertable[2].textContent = "Добавить нового пользователя";
    }
    else {
        usertable[2].textContent = "Add new user";
    }

    usertable[4].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[4].textContent = "Имя клиента";
    }
    else {
        usertable[4].textContent = "Username";
    }
    usertable[5].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[5].textContent = "Код восстановления";
    }
    else {
        usertable[5].textContent = "Recovery code";
    }
    usertable[6].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[6].textContent = "Эл. почта";
    }
    else {
        usertable[6].textContent = "Email";
    }
    usertable[7].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[7].textContent = "Пароль";
    }
    else {
        usertable[7].textContent = "Password";
    }
    usertable[8].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[8].textContent = "Ур. доступа (1-4)";
    }
    else {
        usertable[8].textContent = "Access level (1-4)";
    }
    usertable[9].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[9].textContent = "Подтверждение";
    }
    else {
        usertable[9].textContent = "Confirm changes";
    }

    usertable[1].appendChild(usertable[2]);
    usertable[3].appendChild(usertable[4]);
    usertable[3].appendChild(usertable[5]);
    usertable[3].appendChild(usertable[6]);
    usertable[3].appendChild(usertable[7]);
    usertable[3].appendChild(usertable[8]);
    usertable[3].appendChild(usertable[9]);
    usertable[0].appendChild(usertable[1]);
    usertable[0].appendChild(usertable[3]);
    admin_panel.appendChild(usertable[0]);


    tableItem = [
        document.createElement('tr'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td')
    ];

    tableItem[0].className = "cart_item";
    tableItem[0].id = "user-new";
    tableItem[1].className = "product-price";
    tableItem[2].className = "edit-product-data";
    tableItem[2].id = "username-new";
    tableItem[2].setAttribute("contenteditable", true);
    tableItem[3].className = "product-price";
    tableItem[4].className = "edit-product-data";
    tableItem[4].id = "restore-new";
    tableItem[4].setAttribute("contenteditable", true);
    tableItem[5].className = "product-price";
    tableItem[6].className = "edit-product-data";
    tableItem[6].id = "email-new";
    tableItem[6].setAttribute("contenteditable", true);
    tableItem[7].className = "product-price";
    tableItem[8].className = "edit-product-data";
    tableItem[8].id = "pass-new";
    tableItem[8].setAttribute("contenteditable", true);
    tableItem[9].className = "product-price";
    tableItem[10].className = "edit-product-data";
    tableItem[10].id = "level-new";
    tableItem[10].setAttribute("contenteditable", true);
    tableItem[11].className = "product-price edit-user";
    if (getCookie("language") == "russian") {
        tableItem[11].textContent = "Добавить";
    }
    else {
        tableItem[11].textContent = "Add User";
    }
    tableItem[11].id = "new-user";
    tableItem[11].onclick = function () {
        client = {
            Username: (document.getElementById('username-new')).textContent,
            Restore_code: (document.getElementById('restore-new')).textContent,
            Email: (document.getElementById('email-new')).textContent,
            Password: (document.getElementById('pass-new')).textContent,
            Access_lvl: (document.getElementById('level-new')).textContent
        }
        if (client.Username != ""
            && client.Name != ""
            && client.Email != ""
            && client.Password != ""
            && client.Access_lvl != "") {

            $.ajax({
                url: 'api/customers',
                type: 'POST',
                data: client,
                success: function (result) {
                    document.location.reload();
                }
            });
        }
    };
    tableItem[1].appendChild(tableItem[2]);
    tableItem[3].appendChild(tableItem[4]);
    tableItem[5].appendChild(tableItem[6]);
    tableItem[7].appendChild(tableItem[8]);
    tableItem[9].appendChild(tableItem[10]);
    tableItem[0].appendChild(tableItem[1]);
    tableItem[0].appendChild(tableItem[3]);
    tableItem[0].appendChild(tableItem[5]);
    tableItem[0].appendChild(tableItem[7]);
    tableItem[0].appendChild(tableItem[9]);
    tableItem[0].appendChild(tableItem[11]);
    usertable[0].appendChild(tableItem[0]);

}

var addProduct = function () {
    usertable = [
        document.createElement('table'),
        document.createElement('tr'),
        document.createElement('th'),
        document.createElement('tr'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th'),
        document.createElement('th')
    ];

    usertable[0].className = "shop_table cart";
    if (Number(getCookie("accLvl")) != 3 && Number(getCookie("accLvl")) != 4) {
        usertable[0].style.display = "none";
    }
    usertable[2].setAttribute("colspan", 5);
    if (getCookie("language") == "russian") {
        usertable[2].textContent = "Добавить новый товар";
    }
    else {
        usertable[2].textContent = "Add new item";
    }

    usertable[4].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[4].textContent = "Название";
    }
    else {
        usertable[4].textContent = "Name";
    }
    usertable[5].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[5].textContent = "Цена";
    }
    else {
        usertable[5].textContent = "Price";
    }
    usertable[6].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[6].textContent = "Описание";
    }
    else {
        usertable[6].textContent = "Description";
    }
    usertable[7].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[7].textContent = "Категория";
    }
    else {
        usertable[7].textContent = "Category";
    }
    usertable[8].className = "product-price td-width";
    if (getCookie("language") == "russian") {
        usertable[8].textContent = "Подтверждение";
    }
    else {
        usertable[8].textContent = "Confirm changes";
    }

    usertable[1].appendChild(usertable[2]);
    usertable[3].appendChild(usertable[4]);
    usertable[3].appendChild(usertable[5]);
    usertable[3].appendChild(usertable[6]);
    usertable[3].appendChild(usertable[7]);
    usertable[3].appendChild(usertable[8]);
    usertable[0].appendChild(usertable[1]);
    usertable[0].appendChild(usertable[3]);
    admin_panel.appendChild(usertable[0]);


    tableItem = [
        document.createElement('tr'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td'),
        document.createElement('div'),
        document.createElement('td')
    ];

    tableItem[0].className = "cart_item";
    tableItem[1].className = "item-price";
    tableItem[2].className = "edit-item-data";
    tableItem[2].id = "item-name-new";
    tableItem[2].setAttribute("contenteditable", true);
    tableItem[3].className = "item-price";
    tableItem[4].className = "edit-item-data";
    tableItem[4].id = "item-price-new";
    tableItem[4].setAttribute("contenteditable", true);
    tableItem[5].className = "item-price";
    tableItem[6].className = "edit-item-data";
    tableItem[6].id = "item-desc-new";
    tableItem[6].setAttribute("contenteditable", true);
    tableItem[7].className = "item-price";
    tableItem[8].className = "edit-item-data";
    tableItem[8].id = "item-category-new";
    tableItem[8].setAttribute("contenteditable", true);
    tableItem[9].className = "item-price edit-user";
    if (getCookie("language") == "russian") {
        tableItem[9].textContent = "Добавить";
    }
    else {
        tableItem[9].textContent = "Add Item";
    }
    tableItem[9].id = "new-item";
    tableItem[9].onclick = function () {
        var item_category = (document.getElementById('item-category-new')).textContent;

        
        for (var cat_index = 0; cat_index < categories.length; cat_index++) {
            if ((categories[cat_index].Name).toLowerCase() == item_category.toLowerCase()) {
                item_category = categories[cat_index].CategoryId;
                break;
            }
        }
        if ((document.getElementById('item-name-new')).textContent != ""
            && (document.getElementById('item-price-new')).textContent != ""
            && (document.getElementById('item-desc-new')).textContent != ""
            && (document.getElementById('item-category-new')).textContent != "") {

            var new_item_price = (document.getElementById('item-price-new')).textContent;
            if (new_item_price.indexOf("$") != 0)
                new_item_price = new_item_price.replace(/\$/g, "");
            
            if (typeof (item_category) != "number") {
                item_category = item_category.toLowerCase();
                item_category = item_category.replace(item_category[0], item_category[0].toUpperCase());

                $.post('api/categories', { Name: item_category, Description: item_category })
                    .done(function () {
                        $.getJSON('api/categories')
                            .done(function (categ_list) {
                                categ_list.forEach(function (value) {
                                    if (value.Name == item_category) {
                                        item_category = value.CategoryId;
                                    }
                                });

                                new_item = {
                                    Name: (document.getElementById('item-name-new')).textContent,
                                    Price: new_item_price,
                                    Description: (document.getElementById('item-desc-new')).textContent,
                                    CategoryId: item_category
                                }

                                $.ajax({
                                    url: 'api/products',
                                    type: 'POST',
                                    data: new_item,
                                    success: function (result) {
                                        document.location.reload();
                                    }
                                });
                            });
                    });
            }
            else {
                new_item = {
                    Name: (document.getElementById('item-name-new')).textContent,
                    Price: new_item_price,
                    Description: (document.getElementById('item-desc-new')).textContent,
                    CategoryId: item_category
                }
                $.ajax({
                    url: 'api/products',
                    type: 'POST',
                    data: new_item,
                    success: function (result) {
                        document.location.reload();
                    }
                });
            }
        }
    };
    tableItem[1].appendChild(tableItem[2]);
    tableItem[3].appendChild(tableItem[4]);
    tableItem[5].appendChild(tableItem[6]);
    tableItem[7].appendChild(tableItem[8]);
    tableItem[0].appendChild(tableItem[1]);
    tableItem[0].appendChild(tableItem[3]);
    tableItem[0].appendChild(tableItem[5]);
    tableItem[0].appendChild(tableItem[7]);
    tableItem[0].appendChild(tableItem[9]);
    usertable[0].appendChild(tableItem[0]);
}

var buildNewsTable = function () {
    newstable = [
        document.createElement("table"),
        document.createElement("tr"),
        document.createElement("th"),
        document.createElement("tr"),
        document.createElement("th"),
        document.createElement("th"),
        document.createElement("th"),
        document.createElement("th")
    ];
    newstable[0].className = "shop_table cart";
    newstable[0].id = "table_news";
    if (Number(getCookie("accLvl")) != 2 && Number(getCookie("accLvl")) != 4) {
        newstable[0].style.display = "none";
    }
    newstable[2].setAttribute("colspan", 4);
    if (getCookie("language") == "russian") {
        newstable[2].textContent = "Публикации";
    }
    else {
        newstable[2].textContent = "Posts";
    }
    newstable[4].className = "item-remove";
    if (getCookie("language") == "russian") {
        newstable[4].textContent = "Удалить";
    }
    else {
        newstable[4].textContent = "Remove";
    }
    newstable[5].className = "item-data";
    if (getCookie("language") == "russian") {
        newstable[5].textContent = "Номер";
    }
    else {
        newstable[5].textContent = "Post ID";
    }
    newstable[6].className = "item-data";
    if (getCookie("language") == "russian") {
        newstable[6].textContent = "Дата публикации";
    }
    else {
        newstable[6].textContent = "Post's Date";
    }
    newstable[7].className = "item-bigdata";
    if (getCookie("language") == "russian") {
        newstable[7].textContent = "Текст поста";
    }
    else {
        newstable[7].textContent = "Post's Text";
    }

    newstable[1].appendChild(newstable[2]);
    newstable[3].appendChild(newstable[4]);
    newstable[3].appendChild(newstable[5]);
    newstable[3].appendChild(newstable[6]);
    newstable[3].appendChild(newstable[7]);
    newstable[0].appendChild(newstable[1]);
    newstable[0].appendChild(newstable[3]);

    $.getJSON("api/news")
        .done(function (data) {
            data.forEach(function (value) {
                newsitem = [
                    document.createElement('tr'),
                    document.createElement('td'),
                    document.createElement('a'),
                    document.createElement('td'),
                    document.createElement('div'),
                    document.createElement('td'),
                    document.createElement('div'),
                    document.createElement('td'),
                    document.createElement('div')
                ];

                newsitem[0].className = "cart_item";
                newsitem[0].id = "post" + value.NewsId;
                newsitem[1].className = "item-remove";
                newsitem[2].className = "remove";
                if (getCookie("language") == "russian") {
                    newsitem[2].title = "Удалить";
                }
                else {
                    newsitem[2].title = "Remove";
                }
                newsitem[2].id = "remove_item" + value.NewsId;
                newsitem[2].textContent = "X";
                newsitem[2].onclick = function () {
                    post_id = (this.id).substring(11);
                    $.ajax({
                        url: 'api/news/' + post_id,
                        type: 'DELETE',
                        success: function (result) {
                            tablepost = document.getElementById("post" + post_id);
                            table_news.removeChild(tablepost);
                        }
                    });
                };
                newsitem[3].className = "item-data";
                newsitem[4].textContent = value.NewsId;
                newsitem[5].className = "item-data";
                newsitem[6].textContent = value.Date;
                newsitem[7].className = "item-bigdata";
                newsitem[8].textContent = value.Text;

                newsitem[1].appendChild(newsitem[2]);
                newsitem[3].appendChild(newsitem[4]);
                newsitem[5].appendChild(newsitem[6]);
                newsitem[7].appendChild(newsitem[8]);
                newsitem[0].appendChild(newsitem[1]);
                newsitem[0].appendChild(newsitem[3]);
                newsitem[0].appendChild(newsitem[5]);
                newsitem[0].appendChild(newsitem[7]);
                newstable[0].appendChild(newsitem[0]);
            });
        });

    admin_panel.appendChild(newstable[0]);
}

var orders_acception = function () {
    var proc_orders = [];
    orders_full.forEach(function (value) {
        if (value.Status == "In processing")
            proc_orders.push(value);
    });

    if (proc_orders.length != 0) {

        usertable = [
            document.createElement('table'),
            document.createElement('tr'),
            document.createElement('th'),
            document.createElement('tr'),
            document.createElement('th'),
            document.createElement('th'),
            document.createElement('th'),
            document.createElement('th'),
            document.createElement('th')
        ];

        usertable[0].className = "shop_table cart";
        if (Number(getCookie("accLvl")) != 3 && Number(getCookie("accLvl")) != 4) {
            usertable[0].style.display = "none";
        }
        usertable[2].setAttribute("colspan", 6);
        if (getCookie("language") == "russian") {
            usertable[2].textContent = "Заказы ожидающие подтверждения";
        }
        else {
            usertable[2].textContent = "Pending Orders";
        }

        usertable[4].className = "product-price td-width";
        if (getCookie("language") == "russian") {
            usertable[4].textContent = "Номер заказа";
        }
        else {
            usertable[4].textContent = "Order Id";
        }
        usertable[5].className = "product-price td-width";
        if (getCookie("language") == "russian") {
            usertable[5].textContent = "Номер заказчика";
        }
        else {
            usertable[5].textContent = "User Id";
        }
        usertable[6].className = "product-price td-width";
        if (getCookie("language") == "russian") {
            usertable[6].textContent = "Номера товаров содержащихся в заказе";
        }
        else {
            usertable[6].textContent = "Ids of items contained in the order";
        }
        usertable[7].className = "product-price td-width";
        if (getCookie("language") == "russian") {
            usertable[7].textContent = "Дата заказа";
        }
        else {
            usertable[7].textContent = "Order Date";
        }
        usertable[8].className = "product-price td-width";
        usertable[8].setAttribute("colspan", 2);
        if (getCookie("language") == "russian") {
            usertable[8].textContent = "Подтверждение";
        }
        else {
            usertable[8].textContent = "Confirm changes";
        }

        usertable[1].appendChild(usertable[2]);
        usertable[3].appendChild(usertable[4]);
        usertable[3].appendChild(usertable[5]);
        usertable[3].appendChild(usertable[6]);
        usertable[3].appendChild(usertable[7]);
        usertable[3].appendChild(usertable[8]);
        usertable[0].appendChild(usertable[1]);
        usertable[0].appendChild(usertable[3]);
        admin_panel.appendChild(usertable[0]);

        proc_orders.forEach(function (value) {

            tableItem = [
                document.createElement('tr'),
                document.createElement('td'),
                document.createElement('div'),
                document.createElement('td'),
                document.createElement('div'),
                document.createElement('td'),
                document.createElement('div'),
                document.createElement('td'),
                document.createElement('div'),
                document.createElement('td'),
                document.createElement('td'),
                document.createElement('td')
            ];

            tableItem[0].className = "cart_item";
            tableItem[1].className = "item-price";
            tableItem[2].textContent = value.OrderId;
            tableItem[2].id = "order_id" + value.OrderId;
            tableItem[3].className = "item-price";
            tableItem[4].textContent = value.CustomerId;
            tableItem[4].id = "order_cust_id" + value.OrderId;
            tableItem[5].className = "item-price";
            tableItem[6].textContent = value.Products_id;
            tableItem[6].id = "order_products" + value.OrderId;
            tableItem[7].className = "item-price";
            date = value.Date;
            date = (date.toString()).split("T");
            date[1] = date[1].substring(0, 8);
            date = date[0] + ", " + date[1];
            date = date.replace("-", ".");
            date = date.replace("-", ".");
            tableItem[8].textContent = date;
            tableItem[8].id = "order_date" + value.OrderId;
            tableItem[9].id = "accept" + value.OrderId;
            tableItem[9].className = "item-price edit-user order-buttons";
            if (getCookie("language") == "russian") {
                tableItem[9].textContent = "Принять";
            }
            else {
                tableItem[9].textContent = "Accept";
            }
            tableItem[9].onclick = function () {
                order_id = Number((this.id).substring(6));
                new_order = {
                    OrderId: order_id,
                    CustomerId: (document.getElementById("order_cust_id" + order_id)).textContent,
                    Products_id: (document.getElementById("order_products" + order_id)).textContent,
                    Date: (document.getElementById("order_date" + order_id)).textContent,
                    Price: (document.getElementById("order_price" + order_id)).textContent,
                    Status: "Accepted"
                }
                $.ajax({
                    url: 'api/orders/' + order_id,
                    type: 'PUT',
                    data: new_order,
                    success: function (result) {
                        document.location.reload();
                    }
                });
            };
            tableItem[10].id = "decline" + value.OrderId;
            tableItem[10].className = "item-price edit-user order-buttons button-decline";
            if (getCookie("language") == "russian") {
                tableItem[10].textContent = "Отказать";
            }
            else {
                tableItem[10].textContent = "Decline";
            }
            tableItem[10].onclick = function () {
                order_id = Number((this.id).substring(7));
                new_order = {
                    OrderId: order_id,
                    CustomerId: (document.getElementById("order_cust_id" + order_id)).textContent,
                    Products_id: (document.getElementById("order_products" + order_id)).textContent,
                    Date: (document.getElementById("order_date" + order_id)).textContent,
                    Price: (document.getElementById("order_price" + order_id)).textContent,
                    Status: "Declined"
                }
                $.ajax({
                    url: 'api/orders/' + order_id,
                    type: 'PUT',
                    data: new_order,
                    success: function (result) {
                        document.location.reload();
                    }
                });
            };
            tableItem[11].textContent = value.Price;
            tableItem[11].id = "order_price" + value.OrderId;
            tableItem[11].style.display = "none";
            tableItem[1].appendChild(tableItem[2]);
            tableItem[3].appendChild(tableItem[4]);
            tableItem[5].appendChild(tableItem[6]);
            tableItem[7].appendChild(tableItem[8]);
            tableItem[0].appendChild(tableItem[1]);
            tableItem[0].appendChild(tableItem[3]);
            tableItem[0].appendChild(tableItem[5]);
            tableItem[0].appendChild(tableItem[7]);
            tableItem[0].appendChild(tableItem[9]);
            tableItem[0].appendChild(tableItem[10]);
            tableItem[0].appendChild(tableItem[11]);
            usertable[0].appendChild(tableItem[0]);

        });
    }
}

function goToRussian() {
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";

    label_1.textContent = "Мои данные";
    label_2.textContent = "Смена пароля";
    label_3.textContent = "Заказы";
    label_4.textContent = "Спец. меню";
    txt_1_fname.textContent = "Полное имя";
    txt_1_username.textContent = "Имя пользователя";
    txt_1_email.textContent = "Электронная почта";
    txt_2_pass.textContent = "Пароль";
    txt_2_new_pass.textContent = "Новый пароль";
    change_pass.textContent = "Cменить пароль";

}