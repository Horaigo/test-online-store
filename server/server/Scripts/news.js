﻿if (Number(getCookie("accLvl")) == 4 || Number(getCookie("accLvl")) == 2) {
    post_button.style.display = "block";
}

if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

$.getJSON("api/news")
    .done(function (data) {
        for (i = data.length - 1; i > -1; i--) {
            newsBlock = document.createElement('div');
            newsBlock.className = "news-item";
            headBlock = document.createElement('div');
            headBlock.className = "news-item-head";
            bodyBlock = document.createElement('div');
            bodyBlock.className = "news-item-body";
            date = data[i].Date;
            date = (date.toString()).split("T");
            date[1] = date[1].substring(0, 8);
            date = date[0] + ", " + date[1];
            date = date.replace("-", ".");
            date = date.replace("-", ".");
            headBlock.textContent = date;
            bodyBlock.innerHTML = data[i].Text;
            newsBlock.appendChild(headBlock);
            newsBlock.appendChild(bodyBlock);
            board.appendChild(newsBlock);
        }
    });
var goToEditor = function () {
    document.location.href = "editor.html"
}

function goToRussian () {
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";

    dayreturn.textContent = "30 дней для возврата";
    shipping.textContent = "Бесплатная доставка";
    securepay.textContent = "Безопасная оплата";
    newitems.textContent = "Новые товары";
    posttitle.textContent = "Последние новости";
    post_button.textContent = "Добавить новый пост";
}