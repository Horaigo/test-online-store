﻿if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

var id = location.search.substring(1)

$.getJSON('api/products/'+id)
    .done(function (data) {
        category = document.getElementById("category");
        name_list = document.getElementById("name_list");
        image = document.getElementById("image");
        name_desc = document.getElementById("name_desc");
        category_desc = document.getElementById("category_desc");
        description = document.getElementById("description");
        price = document.getElementById("price");
        if (getCookie("language") == "russian") {
            switch (data.Category) {
                case 'GPU':
                    category.textContent = " Видеокарта";
                    break;
                case 'CPU':
                    category.textContent = " Процессор";
                    break;
                case 'Computer Cases':
                    category.textContent = " Корпуса ПК";
                    break;
                case 'Motherboards':
                    category.textContent = " Материнские платы";
                    break;
                case 'RAM':
                    category.textContent = " Оперативная память";
                    break;
                case 'Power Supply':
                    category.textContent = " Блоки питания";
                    break;
                default:
                    category.textContent = data.Category;
                    break;
            }
        }
        else {
            category.textContent = data.Category;
        }
        category_desc.textContent = category.textContent;
        category_desc.onclick = function () {
            document.cookie = "category=" + data.Category + ";path=/;";
            window.location.href = "../shop.html";
        };
        category.onclick = category_desc.onclick;
        name_list.textContent = data.Name;
        image.src = "Images/" + data.Name + ".jpg";
        name_desc.textContent = data.Name;
        description.textContent = data.Description;
        price.textContent = "$"+ data.Price;
    });

function goToRussian() {
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";
    home_button.textContent = "Главная";
    category_text.textContent = "Категория:";
    cart_button.textContent = "Добавить в корзину";

}