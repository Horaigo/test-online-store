﻿if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

var checkUserData = function () {
    username = document.getElementById("username");
    username = username.value;
    password = document.getElementById("password");
    password = password.value;
    password_alert = document.getElementById("password_alert");
    password_alert.style.visibility = "hidden"
    username_alert = document.getElementById("username_alert");
    username_alert.style.visibility = "hidden";
    var user = checkUser(username);
    user.then(
        result => {
            if (result != null) {

                if (result.Password == password) {
                    document.cookie = "userId=" + result.CustomerId + ";path=/;";
                    document.cookie = "accLvl=" + result.Access_lvl + ";path=/;";
                    document.location.href = "../index.html";
                }
                else {
                    password_alert.style.visibility = "visible";
                }

            }
            else {
                username_alert.style.visibility = "visible";
            }
        },
        error => {}
    );
}

var checkUser = function (nickname) {
    var promise = new Promise((resolve, reject) => {
        $.getJSON('api/customers')
            .done(function (data) {
                for (i = 0; i < data.length; i++) {
                    if (data[i].Username == nickname) {
                        tmp = data[i];
                        break;
                    }
                    else
                        tmp = null;
                }
                resolve(tmp);
            });
    });
    return promise;
}

function goToRussian() {
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";

    register_title.textContent = "Зарегистрироваться";
    login_title.textContent = "Войти";
    reg_first_str.textContent = "Впервые здесь?";
    reg_second_str.textContent = "Зарегистрируйтесь чтобы стать полноправным пользователем в нашем магазине";
    reg_button.textContent = "Регистрация";
    login_message.textContent = "Добро пожаловать назад!";
    username_text.style["white-space"] = "nowrap";
    username_text.textContent = "Имя пользователя";
    password_text.textContent = "Пароль";
    login_button.textContent = "Войти";
    rest_pas_text.textContent = "Забыли пароль? Восстановите его здесь!";
    username_alert.textContent = "Пользователя с заданным именем не существует";
    password_alert.textContent = "Неверный пароль";
}