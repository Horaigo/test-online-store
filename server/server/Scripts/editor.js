﻿if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

tinymce.init({
    selector: '#editor',
    menubar: false,
    toolbar: "undo redo | formatselect | bold italic strikethrough| table"
});

var addNews = function () {
    news_text = tinyMCE.activeEditor.getContent();
    $.post("api/news", { Text: news_text, Date: (new Date()).toUTCString() }).done(function () {
            document.location.href = "index.html";
        });
}

var getCurrentDate = function () {
    newsDate = new Date;
    newsDate = (newsDate.toISOString()).split("T");
    newsDate[1] = newsDate[1].substring(0, 8);
    newsDate = newsDate[0] + ", " + newsDate[1];
    newsDate = newsDate.replace("-", ".");
    newsDate = newsDate.replace("-", ".");
    return newsDate;
}

function goToRussian() {
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";

    page_title.textContent = "Добавить новый пост";
    post_text.textContent = "Опубликовать";
}