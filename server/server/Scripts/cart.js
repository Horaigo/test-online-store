﻿if (getCookie("language") === undefined) {
    document.cookie = "language=english;path=/;";
}

if (getCookie("language") == "russian") {
    goToRussian();
}

if (getCookie("cartItems") === undefined) {
    checkout_button.style.visibility = "hidden";
    tableitem = document.createElement('th');
    tableitem.setAttribute("colspan", 6);
    tableitem.style.background = "#FFF";
    tableitem.style["font-size"] = "small";
    if (getCookie("language") == "russian") {
        tableitem.textContent = "Вы еще ничего не добавили в корзину.";
    } else {
        tableitem.textContent = "You have not added anything to your cart yet.";
    }
    table_body.appendChild(tableitem);
}
else {
    $.getJSON('api/products')
        .done(function (data) {
            cart_items = getCookie("cartItems");
            cart_items = cart_items.split(",");

            var result = cart_items.reduce(function (acc, el) {
                acc[el] = (acc[el] || 0) + 1;
                return acc;
            }, {});

            for (key in result) {
                item = data[key-1];
                tableItem = [
                    document.createElement('tr'),
                    document.createElement('td'),
                    document.createElement('a'),
                    document.createElement('td'),
                    document.createElement('img'),
                    document.createElement('td'),
                    document.createElement('span'),
                    document.createElement('td'),
                    document.createElement('span'),
                    document.createElement('td'),
                    document.createElement('span'),
                    document.createElement('td'),
                    document.createElement('span')
                ];
                tableItem[0].className = "cart_item";
                tableItem[0].id = "item" + item.ProductId;
                tableItem[1].className = "product-remove";
                tableItem[2].className = "remove";
                tableItem[2].title = "Remove this item";
                tableItem[2].textContent = "X";
                tableItem[2].onclick = function () {
                    cart_cookies = [(getCookie("cartItems")).split(","), Number(getCookie("cartPrice")), Number(getCookie("cartCount"))];
                    cart_cookies[0] = cart_cookies[0].filter(function (number) { return number != item.ProductId });
                    cart_cookies[1] = cart_cookies[1] - result[key] * item.Price;
                    cart_cookies[2] = cart_cookies[2] - result[key];
                    if (cart_cookies[0].toString() == "" || cart_cookies[1] == 0 || cart_cookies[2] == 0) {
                        deleteCookie("cartItems");
                        deleteCookie("cartPrice");
                        deleteCookie("cartCount");
                    }
                    else {
                        document.cookie = "cartItems=" + cart_cookies[0].toString() + ";path=/;";
                        document.cookie = "cartPrice=" + cart_cookies[1].toString() + ";path=/;";
                        document.cookie = "cartCount=" + cart_cookies[2].toString() + ";path=/;";
                    }
                    tmp = document.getElementById("item" + item.ProductId);
                    tmp.remove();
                    document.location.reload();
                };
                tableItem[3].className = "product-thumbnail";
                tableItem[4].className = "shop_thumbnail";
                tableItem[4].src = "Images/" + item.Name + ".jpg";
                tableItem[4].onclick = function () { document.location.href = "../product.html?" + item.ProductId; };
                tableItem[5].className = "product-name";
                tableItem[6].textContent = item.Name;
                tableItem[6].onclick = function () { document.location.href = "../product.html?" + item.ProductId; };
                tableItem[7].className = "product-price";
                tableItem[8].className = "amount";
                tableItem[8].textContent = "$" + item.Price + ".00";
                tableItem[9].className = "product-quantity";
                tableItem[10].className = "amount";
                tableItem[10].textContent = result[key];
                tableItem[11].className = "product-subtotal";
                tableItem[12].className = "amount";
                tableItem[12].textContent = "$" + (result[key] * item.Price) + ".00";
                tableItem[1].appendChild(tableItem[2]);
                tableItem[3].appendChild(tableItem[4]);
                tableItem[5].appendChild(tableItem[6]);
                tableItem[7].appendChild(tableItem[8]);
                tableItem[9].appendChild(tableItem[10]);
                tableItem[11].appendChild(tableItem[12]);
                tableItem[0].appendChild(tableItem[1]);
                tableItem[0].appendChild(tableItem[3]);
                tableItem[0].appendChild(tableItem[5]);
                tableItem[0].appendChild(tableItem[7]);
                tableItem[0].appendChild(tableItem[9]);
                tableItem[0].appendChild(tableItem[11]);
                table_body.appendChild(tableItem[0]);
            }
            total_price.textContent = "$"+getCookie("cartPrice")+".00";
        });
}

var doCheckout = function () {
    $.post('api/orders', { Products_id: getCookie("cartItems"), CustomerId: getCookie("userId"), Price: getCookie("cartPrice"), Date: (new Date()).toUTCString(), Status: "In processing" })
        .done(function () {
            deleteCookie("cartItems");
            deleteCookie("cartCount");
            deleteCookie("cartPrice");
            document.location.reload();
        });
}
var hideCheckout = function () {
    if (getCookie("cartItems") === undefined || getCookie("userId") === undefined)
        checkout_button.style.visibility = "hidden";
}
var getUnique = function (arr) {
    var rez = [];
    rez.push(arr[0]);
    arr.forEach(function (item, i) {
        if (!(rez.indexOf(item) + 1))
            rez.push(item);
    });
    return rez.length;
}

function goToRussian() {
    (document.getElementById("header-cart")).textContent = "Корзина";
    reglog.textContent = "Регистрация/Вход";
    account.textContent = "Личный кабинет";
    language.textContent = "Язык";
    logout_button.textContent = "Выход";
    (document.getElementById("home-menu")).textContent = "Главная";
    (document.getElementById("shop-menu")).textContent = "Магазин";
    (document.getElementById("cart-menu")).textContent = "Корзина";
    checkout_button.value = "Оформить заказ";
    cart_name.textContent = "Товар";
    cart_item_price.textContent = "Цена";
    cart_quantity.textContent = "Количество";
    cart_subtotal.textContent = "Общая сумма";
    total.textContent = "Всего";
}