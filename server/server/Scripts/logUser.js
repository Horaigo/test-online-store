﻿if (getCookie("userId") !== undefined) {
    (document.getElementById("header-cart")).style.display = "block";
    account.style.display = "block";
    (document.getElementById("logout")).style.visibility = "visible";
    (document.getElementById("cart-view")).style.display = "block";
    (document.getElementById("shop-menu")).style.display = "block";
    (document.getElementById("cart-menu")).style.display = "block";
} else {
    reglog.style.display = "block";
}
var logout = document.getElementById("logout");

if (getCookie("cartItems") !== undefined) {
    cart_price.textContent = "$" + getCookie("cartPrice");
    cart_count.textContent = getCookie("cartCount");
}

logout.onclick = function () {
    deleteCookie("userId");
    deleteCookie("cartItems");
    deleteCookie("cartPrice");
    deleteCookie("cartCount");
    deleteCookie("accLvl");
    document.location.href = "index.html";
};

english.onclick = function () {
    document.cookie = "language=english;path=/;";
    document.location.reload();
}

russian.onclick = function () {
    document.cookie = "language=russian;path=/;";
    document.location.reload();
}