﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using server.Models;

namespace server.IServices
{
    public interface INewsService
    {
        IQueryable<News> GetAllNews();
        ShopContext GetShopContext();
    }
}