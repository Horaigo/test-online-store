namespace server.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Collections.Generic;
    using System.Linq;
    using server.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<server.Models.ShopContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(server.Models.ShopContext context)
        {
            var categories = new List<Category>
            {
               new Category { CategoryId = 1, Name = "GPU", Description = "Graphic Cards" },
               new Category { CategoryId = 2, Name = "CPU", Description = "Proccessors" },
               new Category { CategoryId = 3, Name = "Power Supply", Description = "Power Supply" },
               new Category { CategoryId = 4, Name = "RAM", Description = "RAM" },
               new Category { CategoryId = 5, Name = "Computer Cases", Description = "Computer Cases" },
               new Category { CategoryId = 6, Name = "Motherboards", Description = "Motherboards" }
            };
            categories.ForEach(s => context.Categories.AddOrUpdate(p => p.CategoryId, s));
            context.SaveChanges();

            var customers = new List<Customer> {
               new Customer { CustomerId = 1, Restore_code = "Mike", Access_lvl = 4, Email = "mike@gmail.com", Username = "Mikey", Password = "tasty" },
               new Customer { CustomerId = 2, Restore_code = "Alex", Access_lvl = 3, Email = "alex@gmail.com", Username = "Alexandro", Password = "Alex123" },
               new Customer { CustomerId = 3, Restore_code = "Kate", Access_lvl = 2, Email = "kate@gmail.com", Username = "Katey", Password = "Kate123" },
               new Customer { CustomerId = 4, Restore_code = "Bill", Access_lvl = 1, Email = "bill@gmail.com", Username = "Billy", Password = "Bill123" }
            };
            customers.ForEach(s => context.Customers.AddOrUpdate(p => p.CustomerId, s));
            context.SaveChanges();

            var products = new List<Product> {
               new Product {
                   ProductId = 1,
                   Name = "Aerocool Gold Miner 1200W",
                   Price = 150,
                   Description = "Is the hashing process running 24/7 on your system? The new Aerocool Gold Miner 1200W power supply with high efficiency of 90% will help you mine more efficiently. The Gold Miner 1200W is equipped with two 80mm fans for supplying and blowing out air.These fans provide continuous air flow that maintains optimum temperature in the power supply. So that the power supply does not overheat, both Gold Miner 1200W fans are constantly running at a maximum speed of 4500 rpm.The durability and reliability of the fans when operating under such a load is ensured by special twin bearings. The Gold Miner 1200W is equipped with AWG16 cables with wires that easily withstand high loads.",
                   CategoryId = categories.Single( i => i.Name == "Power Supply").CategoryId },
               new Product {
                   ProductId = 2,
                   Name = "Aerocool KCAS RGB 550W",
                   Price = 75,
                   Description = "Aerocool KCAS RGB 550W certified to 80 PLUS Gold for maximum efficiency. The RGB backlight of the fan on the power supply is compatible with the backlight control systems of motherboards, including ASUS Aura, GIGABYTE RGB Fusion, and MSI Mystic Light Sync. And the P7-Hub (P7-H1). Installed silent and powerful 12-cm fan with 'smart' speed control system.",
                   CategoryId = categories.Single( i => i.Name == "Power Supply").CategoryId },
               new Product {
                   ProductId = 3,
                   Name = "Aerocool VX 350W Plus",
                   Price = 15,
                   Description = "The power supply Aerocool VX 350W PLUS [VX-350 PLUS], presented in black, will become an integral part of the system unit, from which all its components will be powered. The model is made in the ATX form factor, its dimensions correspond to 140x150x86 mm. The device became the owner of an active power factor corrector, thanks to which it almost completely got rid of the parasitic reactive power. There is support for EPS12V. ATX12V version - 2.3. The Aerocool VX 350W PLUS[VX - 350 PLUS] power supply has good electrical parameters: its rated power is 350 W, of which 300 W is connected to the 12 V line.The input voltage range of the mains must be 230 V.The main power connector is 20 + 4 pin, the processor is powered by a 1x 4 + 4 pin interface. Also, the configuration of this model provides two 15-pin SATA connectors, two 4-pin Molex interfaces and one connector for powering the drive.The main power cable has a length of 50 cm, the processor power cable has a length of 55 cm.",
                   CategoryId = categories.Single( i => i.Name == "Power Supply").CategoryId },
               new Product {
                   ProductId = 4,
                   Name = "Be Quiet Dark Power Pro 11 1000W",
                   Price = 300,
                   Description = "High-quality and reliable power supply Be Quiet DARK POWER PRO 11 1000W [BN254] is the choice of those who assemble a computer system, which will be heavily loaded. The presented model was developed with enviable scrupulousness by gifted engineers of the brand Be Quiet, whose profile is the production of first-class computer equipment using advanced technologies. The power supply unit is implemented together with all the necessary things that you may need during its installation, namely, the mounting kit, cable ties, and the power supply cable. The well - conceived architecture of the Be Quiet DARK POWER PRO 11 1000W[BN254] provides for the presence of a black case, executed in the ATX form factor.The model is equipped with an advanced cooling system, which provides for a 13.5 - centimeter fan.The detachable wires of the device are equipped with extra - strong braid, which will significantly prolong its service life.Power supply is a record 1000 watts.",
                   CategoryId = categories.Single( i => i.Name == "Power Supply").CategoryId },
               new Product {
                   ProductId = 5,
                   Name = "InWin Powerman 500W",
                   Price = 35,
                   Description = "The InWin Powerman 500W [PM-500 80plus] power supply will meet all the needs of an average-performance system unit in a power supply. The power supply unit of this model is equipped with all the necessary cables that must provide power to the processor, video card and all other system components. Effective operation of the power supply is possible with an input voltage of 200-240 V. The InWin Powerman 500W[PM - 500 80plus] power supply belongs to the ATX form factor and has a rated output power of 500 watts.A model of such power can be applied in a home station or office office computer.An active air cooling system with a low - noise 120 - mm fan guarantees a stable heat dissipation for the power supply.The power cord is not included with this power supply, it will have to be purchased separately.",
                   CategoryId = categories.Single( i => i.Name == "Power Supply").CategoryId },
               new Product {
                   ProductId = 6,
                   Name = "Asus AMD Radeon RX VEGA 56 STRIX OC",
                   Price = 750,
                   Description = "The Asus AMD Radeon RX VEGA 56 STRIX video card is a top notch gaming model, equipped with many exclusive ASUS technologies. The cooling system used on it can boast high efficiency due to dust-proof fans with optimized impeller geometry, and the ability to connect 4-pin case fans will ensure the most comfortable temperature for such a powerful device. The Aura Sync backlight system will help make your computer design unforgettable with original lighting effects, while compatible with VR devices, HDMI ports will give gamers the opportunity to plunge into the exciting world of virtual reality. The ROG Strix RX VEGA56 package includes the GPU utilities Tweak II (for configuring and monitoring the parameters of a video card) and XSplit Gamecaster (for recording and broadcasting the game process in real time).",
                   CategoryId = categories.Single( i => i.Name == "GPU").CategoryId },
               new Product {
                   ProductId = 7,
                   Name = "Asus GeForce GT 1030 LP",
                   Price = 90,
                   Description = "The Asus GeForce GT 1030 LP [GT1030-2G-BRK] video card is an excellent option for use in primary and secondary computers. The model is based on the proven GeForce GT 1030 video processor. DirectX 12 and OpenGL 4.5 are supported. 2 GB of GDDR5 video memory is enough to realize all the capabilities of the video adapter. The effective frequency of the graphics chip is relatively high: it is equal to 6008 MHz. The Asus GeForce GT 1030 LP[GT1030 - 2G - BRK] video card is connected using the PCI-E interface. Many users will benefit from the ability to simultaneously connect 2 monitors.DisplayPort and HDMI video interfaces are used to display the image. One of the strengths of the video card is extremely low energy consumption.The power consumption of the device does not exceed 30 watts, and a power supply with a power of 300 watts is recommended for the video adapter to work.The length and thickness of the video card are 169 and 21 mm, respectively.",
                   CategoryId = categories.Single( i => i.Name == "GPU").CategoryId },
               new Product {
                   ProductId = 8,
                   Name = "INNO3D GeForce GTX 1070 iChill Black",
                   Price = 750,
                   Description = "Inno3D iChill is a line of Innovision Multimedia video cards specially adapted for gamers and overclockers. IChill video cards are often the best in their class, they have won many prestigious awards from the leading test laboratories in the world. NVIDIA GeForce GTX 1070 is no exception. The video card is equipped with a closed-loop water cooling system, and numerous aluminum fins and an additional fan are responsible for cooling the voltage regulators. The NVIDIA GP104 chip is based on the Pascal architecture and is manufactured using the 16-nm FinFET process technology.The amount of video memory is 8GB and the connection is made via a 256-bit bus.",
                   CategoryId = categories.Single( i => i.Name == "GPU").CategoryId },
               new Product {
                   ProductId = 9,
                   Name = "Matrox C900",
                   Price = 3100,
                   Description = "The Matrox C-Series family of multi-display graphics cards allows you to connect up to 9 displays or projectors to a single graphics card. C-Series PCI Express x16 cards offer superior reliability, stability, and ease of deployment and are the perfect solution for popular commercial and mission critical systems. They provide multi-screen monitoring capabilities and outstanding performance for use in control centers, digital advertising, business, industry, security systems and embedded systems.",
                   CategoryId = categories.Single( i => i.Name == "GPU").CategoryId },
               new Product {
                   ProductId = 10,
                   Name = "Palit GeForce GTX 1050 Ti DUAL OC",
                   Price = 200,
                   Description = "The Palit GeForce GTX 1050 Ti DUAL OC video card is an excellent solution for a gaming computer that will provide graphics in decent quality. The GeForce GTX 1050 Ti graphics processor will provide fast image movement on the screen. The amount of video memory in 4 GB type GDDR5 indicates productive and productive work of the processor. The memory bus width is 128 bits - an indicator of good memory bandwidth (112 GB / s). Support for DirectX 12 is a guarantee that the game will be even more beautiful, with even greater detail.For high performance meets the effective memory frequency of 7000 MHz. 2 axial fans provide active air cooling, so no defects in the game due to overheating will appear.Palit GeForce GTX 1050 Ti DUAL OC supports connecting up to three monitors simultaneously, so that the player can fully control the situation in the game.",
                   CategoryId = categories.Single( i => i.Name == "GPU").CategoryId },
               new Product {
                   ProductId = 11,
                   Name = "AeroCool Aero-800 White",
                   Price = 65,
                   Description = "If you want to install the components of your gaming computer in a truly stylish case, then the AeroCool Aero-800 White case is exactly the model you need. The case has an average size, which is determined by the size of the Midi-Tower unit. When installing the motherboard, note that the form factor of the Standard-ATX product. It is also important that the maximum allowable size of the video card for installation is 39 cm. The well - designed architecture of the AeroCool Aero - 800 White chassis provides for a lower power supply layout.Baskets for drives are located both along and across the body.The case did not do without the advanced cooling system included in the kit. It is made in the form of two 12 - centimeter fans.There are also three locations for the location of three additional 12 centimeter fans.The side wall is equipped with a mesh window through which you can observe the computer components illuminated in blue.",
                   CategoryId = categories.Single( i => i.Name == "Computer Cases").CategoryId },
               new Product {
                   ProductId = 12,
                   Name = "AeroCool Cs-102 Black",
                   Price = 25,
                   Description = "AeroCool Cs-102 - case for the Mini Tower frame size system. The body is made of steel with a thickness of 0.4 mm. This model allows you to install motherboard size Micro ATX or Mini ITX, three 3.5 'hard drives and two 2.5' drives. The maximum length of the graphics adapter can reach 240 mm, CPU cooler - 150 mm. The front panel AeroCool Cs-102 contains USB 3.0 and 2.0 connectors, as well as connectors for connecting a microphone and headphones.",
                   CategoryId = categories.Single( i => i.Name == "Computer Cases").CategoryId },
               new Product {
                   ProductId = 13,
                   Name = "AeroCool Quartz Pro Black",
                   Price = 160,
                   Description = "Aerocool has included the Quartz Pro model in the range of its computer cases. The case is made in a Full-Tower form factor, capable of accepting motherboards up to the size of Extended ATX. It is also notable for tempered glass panels that cover the side and front faces.",
                   CategoryId = categories.Single( i => i.Name == "Computer Cases").CategoryId },
               new Product {
                   ProductId = 14,
                   Name = "Be Quiet DARK BASE 900 Black",
                   Price = 250,
                   Description = "Be quiet! Dark Base 900: unlimited possibilities, perfect cooling and silence of work. The case was designed for avid enthusiasts and all those who expect the highest standards when it comes to modularity, compatibility and design for their high-end PC systems overclocked and silent liquid-cooled configurations.",
                   CategoryId = categories.Single( i => i.Name == "Computer Cases").CategoryId },
               new Product {
                   ProductId = 15,
                   Name = "SilverStone Precision PS-11 Black",
                   Price = 70,
                   Description = "SilverStone Precision PS11 allows you to install an ATX, microATX motherboard, seven PCI / PCI-E expansion cards up to 41 cm long and 15.5 cm wide, a CPU cooler up to 161 mm high, an ATX power supply unit (up to 225 mm long), several drives (3x 3.5 ', 2x 2.5', 2x 5.25 ') and 120 and 140 mm frame sizes.One 120 - mm 'propeller' mounted on the front panel is included with the case. Version SST-PS11B - W has a window on the side wall.",
                   CategoryId = categories.Single( i => i.Name == "Computer Cases").CategoryId },
               new Product {
                   ProductId = 16,
                   Name = "ASRock B360M-ITX ac",
                   Price = 70,
                   Description = "The ASRock B360M-ITX / ac motherboard has the dimension Mini-ITX and is positioned as the basis for a compact multi-purpose computer that performs the role of a home or gaming station, office machine. It provides for the installation of a processor with a LGA 1151v2 socket - Intel Core i7 / i5 / i3, Pentium, 8th generation. The model is designed for the installation of DDR4 RAM modules and has two slots for them, it will allow to bring the full amount of RAM of the system unit to 32 GB. The serious potential of this small - sized board designed for the assembly of a functional PC makes it a worthy choice.",
                   CategoryId = categories.Single( i => i.Name == "Motherboards").CategoryId },
               new Product {
                   ProductId = 17,
                   Name = "MSI B450I GAMING PLUS AC",
                   Price = 125,
                   Description = "MSI Pro-series motherboards combine high quality workmanship, superb performance and special optimizations aimed at sophisticated users. Reliable, durable and functional - they will become an excellent basis for PCs of any level and will be able to meet all the requirements of even the most demanding professionals. The integrated audio system MSI Audio Boost is the highest quality sound provided by premium components.Fascinating sound with bright dynamics will not leave indifferent a single listener. This MSI motherboard supports M.2 storage drives.High - speed interfaces allow you to fully realize the full potential of devices and provide quite tangible benefits: quick launch of applications and accelerated data loading will help increase the performance of your system.",
                   CategoryId = categories.Single( i => i.Name == "Motherboards").CategoryId },
               new Product {
                   ProductId = 18,
                   Name = "ASUS B250 MINING EXPERT",
                   Price = 125,
                   Description = "The excitement around the cryptocurrency began to gradually subside, but this does not prevent the component manufacturers from producing specialized mining equipment. ASUS introduced the B250 Mining Expert motherboard, which has 18 PCIe x1 slots and one PCIe x16. The motherboard is designed for processors LGA1151, got two slots for DDR4 RAM and four SATA 6 Gb / s ports.Three 24 - pin ATX connectors are provided for power supply. The BIOS of the motherboard allows you to see which PCIe slots are empty or do not work correctly during computer boot.",
                   CategoryId = categories.Single( i => i.Name == "Motherboards").CategoryId },
               new Product {
                   ProductId = 19,
                   Name = "ASUS CROSSHAIR VI HERO (WI-FI AC)",
                   Price = 280,
                   Description = "The ASUS CROSSHAIR VI HERO (WI-FI AC) mainboard is related to the Standard-ATX form factor and can be the basis for building a productive and aesthetic system unit. Socket AM4 allows you to install on it the most advanced processors from AMD. For connecting system components and peripherals, the board can offer 8x SATA 6Gb / s, 4x USB 2.0, 8x USB 3.0, 1x USB 3.1 Type-C and 1x USB 3.1 Type-A interfaces. This model is equipped with a network and sound adapters, a WI-FI network adapter. Two ports for connecting WI-FI antennas are also located on the board. Mat.ASUS CROSSHAIR VI HERO(WI - FI AC) looks original and aesthetically pleasing thanks to the built -in Aura LED backlight system and the ability to connect additional LED strips.The built -in utility controls both the built -in and the connected backlight, offering a choice of different modes of its operation.",
                   CategoryId = categories.Single( i => i.Name == "Motherboards").CategoryId },
               new Product {
                   ProductId = 20,
                   Name = "ASRock Fatal1ty X299 Professional Gaming i9",
                   Price = 475,
                   Description = "The ASRock Fatal1ty X299 Professional Gaming i9 motherboard is designed for a gaming PC and, therefore, combines features that increase its efficiency in the gaming process: power, Internet, memory, sound, VGA card. The form factor of the Standard-ATX board says that it has a width of 244 mm and a height of 305 mm. The model is suitable for installation in Intel processors with an LGA 2066 socket. The motherboard is based on Intel X299 chipset with UEFI support and four SLI / CrossFire X cards. With support for the 10 Gbps network standard, ASRock Fatal1ty X299 Professional Gaming i9 boosts performance.The board supports DDR4 - 4400 MHz memory, guaranteeing extreme performance, for which the design provides eight slots.Stable signal flow and power, low temperature, and high efficiency in games are achieved using this board.The presence of four expansion slots PCI - Ex16 allows you to connect four video cards.",
                   CategoryId = categories.Single( i => i.Name == "Motherboards").CategoryId },
               new Product {
                   ProductId = 21,
                   Name = "A-Data XPG Spectrix D40 RGB [AX4U240038G16-DRS] 16 GB",
                   Price = 235,
                   Description = "A-Data XPG Spectrix D40 RGB memory combines amazing performance and beauty in a single armored heat sink - the perfect solution for enthusiasts who assemble computers on their own. Spectrix D40 memory modules are developed using high-quality chips that go through a rigorous selection process. They use 10-layer PCBs, which improves the quality of signal transmission and retains high stability even in cases when the system operates at high speeds or in intensive multitasking mode.",
                   CategoryId = categories.Single( i => i.Name == "RAM").CategoryId },
               new Product {
                   ProductId = 22,
                   Name = "Crucial [CT16G4RFD424A] 16 GB",
                   Price = 235,
                   Description = "Crucial [CT16G4RFD424A] 16 GB RAM can be used for various computers. The company used in the manufacture of the best components. You will definitely be satisfied with the characteristics, the bar will last for a long period and there will be no problems with it. In Crucial[CT16G4RFD424A] 16 GB, one die is included.The module has a capacity of 16 GB.You can use it as a separate element, RAM is enough for most assemblies. Perhaps the use of other straps, but you need to pre - examine compatibility. The height of the module - 31.25 mm.It is installed in the slot on the motherboard.Installation is as simple as possible and there will be no difficulty with it.The module is powered with a voltage of 1.2 V. Form factor - DIMM.These slats are used for stationary PCs, the element has the appropriate dimensions.To identify compatibility important type of memory - DDR4.Outdated motherboards and processors do not support it, you need to check the possibility of use in your assembly. Clock frequency -2400 MHz.Due to this parameter, you can provide fast data exchange. The processor requires high frequency RAM to quickly perform all operations.",
                   CategoryId = categories.Single( i => i.Name == "RAM").CategoryId },
               new Product {
                   ProductId = 23,
                   Name = "Kingston HyperX FURY Black Series [HX316LC10FB 8] 8 GB",
                   Price = 80,
                   Description = "RAM Kingston HyperX FURY Black Series [HX316LC10FB / 8] is a reliable and high-performance component that will give the gaming computer the power needed for smooth operation even with the most demanding software. The model will be a real boon for a demanding gamer who wants to upgrade his computer a little to enjoy the passage of games that were previously slowed down and hung up. The main advantage of the presented module is the automatic recognition of the platform and independent overclocking to the maximum available frequency. Kingston HyperX FURY Black Series[HX316LC10FB / 8] RAM should be purchased by any gamer, both beginner and more experienced.The model has excellent performance and attractive appearance: the asymmetrical design of the heat sink will add style to the look of your computer.Package includes one RAM disk with 8 GB of memory.The maximum clock frequency of the presented RAM is 1600 MHz.",
                   CategoryId = categories.Single( i => i.Name == "RAM").CategoryId },
               new Product {
                   ProductId = 24,
                   Name = "SODIMM Crucial Ballistix Sport LT [BLS8G4S240FSD] 8 GB",
                   Price = 110,
                   Description = "SODIMM Crucial Ballistix Sport LT [BLS8G4S240FSD] 8 GB RAM is a great solution for a purchase. The bar refers to the game series of the manufacturer. It has excellent performance and stylish design. The appearance of the module will be important if there is a transparent cover in the case. Crucial Ballistix Sport LT[BLS8G4S240FSD] 8 GB RAM belongs to the SODIMM form factor.The module is used for installation in a laptop.The manufacturer guarantees compatibility, and the connection process will not cause problems for the user. Memory Type - DDR4.The module operates at high frequency - 2400 MHz.Due to this, you can achieve excellent speed notebook.The operating system and programs function perfectly, you can run demanding games and applications. The kit includes one bracket.The amount of memory provided is 8 GB.You can use several modules with similar characteristics if you do not have enough RAM. The bar does not overheat under high loads. It also features low energy consumption. The module receives power from the motherboard with a voltage of 1.2 V.",
                   CategoryId = categories.Single( i => i.Name == "RAM").CategoryId },
               new Product {
                   ProductId = 25,
                   Name = "SODIMM Kingston HyperX Impact [HX421S13IBK2 32] 32 GB",
                   Price = 400,
                   Description = "Kingston HyperX Impact [HX421S13IBK2 / 32] 32GB SODIMM RAM is optimal for use as part of a mobile computer designed to use demanding games or programs that require a high level of performance. The package of memory included 2 16-gigabyte modules corresponding to the type of DDR4. Kingston HyperX Impact [HX421S13IBK2 / 32] SODIMM memory uses a clock frequency of 2133 MHz. The bandwidth of the device is 17000 MB / s.The model is characterized by timings 13 - 13 - 13.The manufacturer of the modules used bilateral installation of chips.To power the memory, the voltage of 1.2 V is used: this feature ensures low power consumption.The memory is packed in a box, the design of which allows you to get acquainted with the appearance of the modules before unpacking.",
                   CategoryId = categories.Single( i => i.Name == "RAM").CategoryId },
               new Product {
                   ProductId = 26,
                   Name = "AMD A8-7650K OEM",
                   Price = 80,
                   Description = "The AMD A8-7650K processor has a powerful configuration that allows it to be used as the basis of a personal computer. Installation of this model, made on the architecture of Steamroller, is made in the connector FM2 +. The main elements of the device are a two - level cache memory and four Kaveri cores that process commands at a frequency of 3300 MHz.At the same time, the availability of Turbo Core 3.0 technology and a free multiplier allows this indicator to be varied depending on the loads. To provide two - way information transfer between the PC's RAM and AMD A8-7650K, a controller supporting DDR3 modules of up to 64 GB is used. Achieving the maximum data rate (34.1 GB / s) is possible if the frequency of the RAM is within 1333-2133 MHz. Also in this model, there is a video processor Radeon R7, which processes the graphics before displaying on the monitor, the system bus and the controller PCI-E 3.0.",
                   CategoryId = categories.Single( i => i.Name == "CPU").CategoryId },
               new Product {
                   ProductId = 27,
                   Name = "AMD Ryzen Threadripper 1900X BOX",
                   Price = 525,
                   Description = "The AMD Ryzen Threadripper 1900X 8 processor cores provide an amazing 16 threads of simultaneous multitasking power, while the 20 MB combined cache and an extensive set of I / O modules from the AMD X399 enthusiast platform provide readiness for any task. This is an advanced platform for a wide range of users.",
                   CategoryId = categories.Single( i => i.Name == "CPU").CategoryId },
               new Product {
                   ProductId = 28,
                   Name = "Intel Celeron G4900 OEM",
                   Price = 75,
                   Description = "Intel Celeron processors are classic, reliable processors for entry-level computers. The excellent performance of a PC with an Intel Celeron processor will allow you to easily use multi - tasking to communicate with friends and family, have fun and work.Get impressive graphics performance and quality that you normally expect from Intel processors.",
                   CategoryId = categories.Single( i => i.Name == "CPU").CategoryId },
               new Product {
                   ProductId = 29,
                   Name = "Intel Core i5-8500 OEM",
                   Price = 340,
                   Description = "The 6-core Intel Core i5-8500 OEM processor, which is installed using the LGA 1151-v2 socket, is manufactured using a 14-nanometer process technology. The basis of the model is the core of Coffee Lake S. The base frequency of the device is 3000 MHz. In turbo mode, the frequency can reach 4,100 MHz. One of the features of the processor, which is suitable for a complete set of high-performance universal computers, is a significant amount of cache in the third level: it is equal to 9 MB. The cache volume of the second level is 1.5 MB. The Intel Core i5 - 8500 OEM processor is compatible with DDR4 memory, which can be up to 64 GB.The maximum frequency of RAM is 2666 MHz.The processor includes an Intel UHD Graphics 630 graphics processor.The maximum frequency of the graphics core is 1100 MHz. The processor has an OEM package.This means that you can choose the cooling system based on your own preferences and the tasks facing the computer.When choosing a cooler, please note that the TDP of the processor is 65 W",
                   CategoryId = categories.Single( i => i.Name == "CPU").CategoryId },
               new Product {
                   ProductId = 30,
                   Name = "Intel Xeon E5-2637 v4 OEM",
                   Price = 1200,
                   Description = "The powerful Intel Xeon E5 processor family offers versatility for a variety of workloads. These processors are designed to design data centers that use software-defined infrastructure to achieve maximum performance, efficiency, and flexibility in delivering services for both cloud and traditional applications. Intel Xeon E5 processors support workloads for cloud computing, high performance computing, networking, and storage.",
                   CategoryId = categories.Single( i => i.Name == "CPU").CategoryId }
            };
            foreach (Product e in products)
            {
                var productInDataBase = context.Products.Where( s => s.ProductId == e.ProductId ).SingleOrDefault();
                if (productInDataBase == null)
                {
                    context.Products.Add(e);
                }
            }
            context.SaveChanges();

            var orders = new List<Order> {
               new Order { OrderId = 1,
                   Products_id = "1, 2, 4",
                   CustomerId = customers.Single( i => i.Username == "Mikey").CustomerId,
                   Price = 525,
                   Date = new DateTime(2018, 10, 15, 12, 30, 30),
                   Status ="Accepted" } ,
               new Order { OrderId = 2,
                   Products_id = "5, 7, 24",
                   CustomerId = customers.Single( i => i.Username == "Alexandro").CustomerId,
                   Price = 235,
                   Date = new DateTime(2018, 10, 15, 12, 30, 30),
                   Status = "Declined" },
               new Order { OrderId = 3,
                   Products_id = "8, 9, 21",
                   CustomerId = customers.Single( i => i.Username == "Katey").CustomerId,
                   Price = 4085,
                   Date = new DateTime(2018, 10, 15, 12, 30, 30),
                   Status = "Accepted" },
               new Order { OrderId = 4,
                   Products_id = "4",
                   CustomerId = customers.Single( i => i.Username == "Billy").CustomerId,
                   Price = 300,
                   Date = new DateTime(2018, 10, 15, 12, 30, 30),
                   Status = "In processing" }
            };
            foreach (Order e in orders)
            {
                var orderInDataBase = context.Orders.Where(s => s.OrderId == e.OrderId).SingleOrDefault();
                if (orderInDataBase == null)
                {
                    context.Orders.Add(e);
                }
            }
            context.SaveChanges();

            var news = new List<News> {
                new News { NewsId = 1, Text = "<h1>Welcome to our shop!</h1><h3>We are very glad to see you here! If you want to see out items and buy something, please register in our system!</h3>", Date = new DateTime(2018, 10, 15, 12, 30, 30) },
                new News { NewsId = 2, Text = "<h1>News Testing</h1><h3>1</h3>", Date = new DateTime(2018, 10, 15, 12, 30, 30) },
                new News { NewsId = 3, Text = "<h1>News Testing</h1><h3>2</h3>", Date = new DateTime(2018, 10, 15, 12, 30, 30) },
                new News { NewsId = 4, Text = "<h1>News Testing</h1><h3>3</h3>", Date = new DateTime(2018, 10, 15, 12, 30, 30) },
                new News { NewsId = 5, Text = "<h1>News Testing</h1><h3>4</h3>", Date = new DateTime(2018, 10, 15, 12, 30, 30) }
            };
            news.ForEach(s => context.News.AddOrUpdate(p => p.NewsId, s));
            context.SaveChanges();

            AddOrUpdateProduct(context, "GPU", "Asus AMD Radeon RX VEGA 56 STRIX OC");
            AddOrUpdateProduct(context, "GPU", "Asus GeForce GT 1030 LP");
            AddOrUpdateProduct(context, "GPU", "INNO3D GeForce GTX 1070 iChill Black");
            AddOrUpdateProduct(context, "GPU", "Matrox C900");
            AddOrUpdateProduct(context, "GPU", "Palit GeForce GTX 1050 Ti DUAL OC");

            AddOrUpdateProduct(context, "CPU", "AMD A8-7650K OEM");
            AddOrUpdateProduct(context, "CPU", "AMD Ryzen Threadripper 1900X BOX");
            AddOrUpdateProduct(context, "CPU", "Intel Celeron G4900 OEM");
            AddOrUpdateProduct(context, "CPU", "Intel Core i5-8500 OEM");
            AddOrUpdateProduct(context, "CPU", "Intel Xeon E5-2637 v4 OEM");

            AddOrUpdateProduct(context, "Power Supply", "Aerocool Gold Miner 1200W");
            AddOrUpdateProduct(context, "Power Supply", "Aerocool KCAS RGB 550W");
            AddOrUpdateProduct(context, "Power Supply", "Aerocool VX 350W Plus");
            AddOrUpdateProduct(context, "Power Supply", "Be Quiet Dark Power Pro 11 1000W");
            AddOrUpdateProduct(context, "Power Supply", "InWin Powerman 500W");

            AddOrUpdateProduct(context, "RAM", "A-Data XPG Spectrix D40 RGB [AX4U240038G16-DRS] 16 GB");
            AddOrUpdateProduct(context, "RAM", "Crucial [CT16G4RFD424A] 16 GB");
            AddOrUpdateProduct(context, "RAM", "Kingston HyperX FURY Black Series [HX316LC10FB 8] 8 GB");
            AddOrUpdateProduct(context, "RAM", "SODIMM Crucial Ballistix Sport LT [BLS8G4S240FSD] 8 GB");
            AddOrUpdateProduct(context, "RAM", "SODIMM Kingston HyperX Impact [HX421S13IBK2 32] 32 GB");

            AddOrUpdateProduct(context, "Computer Cases", "AeroCool Aero-800 White");
            AddOrUpdateProduct(context, "Computer Cases", "AeroCool Cs-102 Black");
            AddOrUpdateProduct(context, "Computer Cases", "AeroCool Quartz Pro Black");
            AddOrUpdateProduct(context, "Computer Cases", "Be Quiet DARK BASE 900 Black");
            AddOrUpdateProduct(context, "Computer Cases", "SilverStone Precision PS-11 Black");

            AddOrUpdateProduct(context, "Motherboards", "ASRock B360M-ITX ac");
            AddOrUpdateProduct(context, "Motherboards", "MSI B450I GAMING PLUS AC");
            AddOrUpdateProduct(context, "Motherboards", "ASUS B250 MINING EXPERT");
            AddOrUpdateProduct(context, "Motherboards", "ASUS CROSSHAIR VI HERO (WI-FI AC)");
            AddOrUpdateProduct(context, "Motherboards", "ASRock Fatal1ty X299 Professional Gaming i9");


            AddOrUpdateOrder(context, 1, 1);
            AddOrUpdateOrder(context, 2, 2);
            AddOrUpdateOrder(context, 3, 3);
            AddOrUpdateOrder(context, 4, 4);
        }
        void AddOrUpdateProduct(ShopContext context, string categoryName, string productName)
        {
            var categ = context.Categories.SingleOrDefault(c => c.Name == categoryName);
            var prod = categ.Products.SingleOrDefault(i => i.Name == productName);
            if (prod == null)
                categ.Products.Add(context.Products.Single(i => i.Name == productName));
        }

        void AddOrUpdateOrder(ShopContext context, int customerId, int orderId)
        {
            var customer = context.Customers.SingleOrDefault(c => c.CustomerId == customerId);
            var order = customer.Orders.SingleOrDefault(i => i.OrderId == orderId);
            if (order == null)
                customer.Orders.Add(context.Orders.Single(i => i.OrderId == orderId));
        }
    }
}
